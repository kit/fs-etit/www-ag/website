# Gitlab der Website der FS-ETIT am KIT

Dieses Gitlab-Repository beinhaltet die Markdown Dateien der neuen Website der FS-ETIT, welche mit Hilfe von [HuGo](https://gohugo.io/) in fertiges HTML kompiliert werden, und dann auf unserem Server gehostet werden.

## Wie ändere ich die Inhalte der Website?
Genaueres dazu findest du [hier](../../wikis) im Wiki.
