/*!
 * Dark Mode Switch v1.0.1 (https://github.com/coliff/dark-mode-switch)
 * Copyright 2021 C.Oliff
 * Licensed under MIT (https://github.com/coliff/dark-mode-switch/blob/main/LICENSE)
 */

var darkSwitch = null;
var isDarkMode = false;
function initDarkModeSwitch() {
  isDarkMode = localStorage.getItem("darkSwitch") !== null && localStorage.getItem("darkSwitch") === "dark";
  isDarkMode = localStorage.getItem("darkSwitch") === null ? window.matchMedia('(prefers-color-scheme: dark)').matches : isDarkMode;
  
  darkSwitch = document.getElementById("darkSwitch");
  if (darkSwitch) {
    darkSwitch.addEventListener("click", function () {
      isDarkMode = !isDarkMode;
      resetTheme();
    });
  }
}

if (document.readyState == "loading") {
  document.addEventListener("DOMContentLoaded", initDarkModeSwitch);
} else {
  initDarkModeSwitch();
}

/**
 * Summary: resetTheme checks if the switch is 'on' or 'off' and if it is toggled
 * on it will set the HTML attribute 'data-theme' to dark so the dark-theme CSS is
 * applied.
 * @return {void}
 */
function resetTheme() {
  if (isDarkMode) {
    document.body.setAttribute("data-bs-theme", "dark");
    localStorage.setItem("darkSwitch", "dark");
  } else {
    document.body.removeAttribute("data-bs-theme");
    localStorage.setItem("darkSwitch", "light");
  }
}