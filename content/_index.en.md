---
title: Homepage
header: home_header.webp
---

Welcome to the website of the **Electrical Engineering and Information Technology student council at the Karlsruhe Institute of Technology**. We represent all students of the Faculty of Electrical Engineering and Information Technology (ETIT) at KIT.

The student council of Electrical Engineering and Information Technology is part of the " Constituted Student Body" at KIT. We offer numerous services around the study (of Electrical Engineering and Information Technology, Mechatronics and Information Technology and Medical Engineering).

In case of problems with exams or your internship, we are happy to advise you during our opening hours, but you can also contact the student representatives in BPA and MPA directly.

Especially for new students or students who are interested in studying, we offer advice about studying and Karlsruhe.

If you have questions, suggestions or criticism, just contact us or take part in a student council meeting on Wednesdays at 6 pm.

In order to fulfill our tasks and offer our services, we are always looking for committed people. If you count yourself among them and feel like helping out, just drop by the student council!
