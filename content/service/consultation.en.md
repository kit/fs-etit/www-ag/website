---
title: 'Consultations'
menu:
  main:
    parent: service
---

## Advice from the student council
Exam nerves? No desire to study? Doubts about your studies? Personal problems? Upcoming exam repetition and you don't know what to do? Then visit the student council, we will help you with your problems!
Before you start studying we can sit down with you and talk about the study program and answer all your questions. You can make an appointment via [mail](mailto:info@fs-etit.kit.edu). Shortly before the beginning of your studies you will receive an email with information about the orientation phase (O-Phase) as well as an invitation to various social media groups.
At the very beginning of your studies you will receive important information about your studies during the O-Phase in our information meeting.
If you need to apply for a deadline extension/second repeat, you can make an appointment with our [BPA](mailto:bpa@fs-etit.kit.edu)- and [MPA](mailto:mpa@fs-etit.kit.edu)-referents by mail.

However, we are not the only ones offering on-campus advising. We've put together a list of all the possible advising centers for you in the next few paragraphs.

## For questions about studying
- The [Center for Information and Counseling](https://www.sle.kit.edu/vorstudium/zib.php) is the central study counseling center of KIT, even before you start studying!
- At the [Study Information Day](https://www.sle.kit.edu/vorstudium/studieninformationstag.php) you can get a first insight into studying.
- During your studies you can contact our [Examination Board Advisors](https://www.etit.kit.edu/beratung.php) for questions specific to your study program.
- The [BeratungsWERK](https://www.sw-ka.de/de/beratung/beratungswerk/) of the Studierendenwerk offers general advice for studying in German, English and French.

## For personal problems
- The [Psychotherapeutic Counseling Center](https://www.sw-ka.de/de/beratung/psychologisch/) for students offers personal consultations as well as anonymous counseling by e-mail, and the site includes self-help texts on the most common problems. All services are free of charge.
- The [Nightline Karlsruhe](https://nightline-karlsruhe.de/) is an offer from students for students. If you are looking for a sympathetic ear, you will be in good hands there.

## For legal and social questions
- Stress with your landlord? Or general legal questions? The AStA offers a [legal advice office hour](https://www.asta-kit.de/de/angebote/beratung/rechtsberatung) once a week. Attention. You have to register in advance. A little tip: Take all the documents related to the question with you.
- For all questions concerning your studies, the Studierendenwerk offers the [BeratungsWERK](https://www.sw-ka.de/de/beratung/beratungswerk/) in the foyer of the Mensa. Here you can also get advice in English and French.
- The AStA also offers several [office hours](https://www.asta-kit.de/de/angebote/beratung/sozialberatung) during the week for questions about BAföG, financing your studies, housing, jobs, studying with a handicap, studying with a child, insurance and much more. You can just drop by!
- For questions about BAföG you can go to the BAföG office of the [Studierendenwerk](https://www.sw-ka.de/de/finanzen/bafoeg/).
