---
title: 'Verleih'
menu:
  main:
    parent: service
---

In der Fachschaft kannst du diverse Gegenstände und Geräte gegen eine Kaution ausleihen. Für andere Fachschaften entfällt diese Kaution.

Schreibe einfach frühzeitig eine E-Mail mit den Gegenständen, die du ausleihen möchtest und den Zeitraum, für die du die Gegenstände brauchst an [verleih@fs-etit.kit.edu](mailto:verleih@fs-etit.kit.edu). Der Verleih muss auf der Fachschaftssitzung, die jeden Mittwoch um 18 Uhr stattfindet, bestätigt werden. Daher solltest du, falls du kurzfristig etwas benötigst, am besten persönlich auf der Sitzung vorbeischauen.

Was wir verleihen, findest du im [AStA-Wiki](https://wiki.asta-kit.de/verleih:start#fachschaft_etit).