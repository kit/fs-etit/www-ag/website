---
title: Opening Hours
menu:
    main:
        parent: service
hideDate: true
---

{{< calendar ics="https://caldav.fs-etit.kit.edu/public.php/public/openinghours/" >}}