---
title: Öffnungszeiten
menu:
    main:
        parent: service
hideDate: true
slug: oeffnungszeiten
---

{{< calendar ics="https://caldav.fs-etit.kit.edu/public.php/public/openinghours/" >}}