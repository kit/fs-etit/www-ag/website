---
title: 'Beratungsangebote'
menu:
  main:
    parent: service
---

## Beratung von Seite der Fachschaft
Prüfungsangst? Keine Lust zum Lernen? Zweifel am Studium? Persönliche Probleme? Anstehende Prüfungswiederholung und du weißt nicht weiter? Dann such die Fachschaft auf, wir helfen euch mit euren Problemen!
Vor dem Studium können wir uns mit euch zusammensetzen und über den Studiengang reden, sowie alle eure Fragen beantworten. Einen Termin könnt ihr per [Mail](mailto:info@fs-etit.kit.edu) vereinbaren. Kurz vor Studienbeginn bekommt ihr von uns eine Mail mit Informationen über die Orientierungsphase (O-Phase) sowie eine Einladung zu verschieden Social Media Gruppen.
Ganz am Anfang des Studiums werdet ihr während der O-Phase in unserer Informationsveranstaltung wichtige Informationen fürs Studium bekommen.
Wenn ihr einen Antrag auf Fristverlängerung/Zweitwiederholung stellen musst, könnt ihr einen Termin mit unseren [BPA](mailto:bpa@fs-etit.kit.edu)-und [MPA](mailto:mpa@fs-etit.kit.edu)-Referenten per Mail vereinbaren.

Wir sind aber nicht die einzigen, die Beratung im Campus anbieten. Wir haben für euch in den nächsten Absätzen eine Liste mit allen möglichen Beratungsstellen aufgestellt.

## Bei Fragen über das Studium
- Das [Zentrum für Information und Beratung](https://www.sle.kit.edu/vorstudium/zib.php) ist die zentrale Studienberatungsstelle des KITs, auch vor dem Studium!
- Am [Studieninformationstag](https://www.sle.kit.edu/vorstudium/studieninformationstag.php) könnt ihr einen ersten Einblick ins Studium werfen.
- Im Studium könnt ihr euch an unsere [Prüfungsauschussreferent:innen](https://www.etit.kit.edu/beratung.php) für studiengangsspezifische Fragen wenden.
- Das [BeratungsWERK](https://www.sw-ka.de/de/beratung/beratungswerk/) des Studierendenwerkes bietet allgemeine Beratung fürs Studium auf Deutsch, Englisch und Französisch.

## Bei persönlichen Problemen
- Die [Psychotherapeutische Beratungsstelle](https://www.sw-ka.de/de/beratung/psychologisch/) für Studierende bietet neben persönlichen Gesprächen auch eine anonyme Beratung per E-Mail an, und auf der Seite finden sich Selbsthilfetexte zu den häufigsten Problemen. Alle Angebote sind kostenlos.
-  Die [Nightline Karlsruhe](https://nightline-karlsruhe.de/) ist ein Angebot von Studierenden für Studierende. Wenn du ein offenes Ohr suchst, wirst du dort gut aufgehoben sein.

## Bei rechtlichen und sozialen Fragen
- Stress mit dem Vermieter? Oder allgemein rechtliche Fragen? Der AStA bietet eine [Rechtsberatungssprechstunde](https://www.asta-kit.de/de/angebote/beratung/rechtsberatung) einmal in der Woche. Achtung! Ihr müsst euch vorher anmelden. Kleiner Tipp: Direkt alle Unterlagen zu der Frage mitnehmen.
- Für alle Fragen rund ums Studium bietet das Studierendenwerk das [BeratungsWERK](https://www.sw-ka.de/de/beratung/beratungswerk/) im Foyer der Mensa an. Hier könnt ihr auch Beratung auf Englisch und Französisch bekommen.
- Der AStA bietet auch in der Woche mehrere [Sprechstundenzeiten](https://www.asta-kit.de/de/angebote/beratung/sozialberatung) zu Fragen rund um BAföG, Studienfinanzierung, Wohnen, Jobben, Studieren mit Handicap, Studieren mit Kind, Versicherung und noch mehr. Ihr könnt einfach vorbeikommen!
- Für Fragen über BAföG könnt ihr zum BAföG-Büro des [Studierendenwerks](https://www.sw-ka.de/de/finanzen/bafoeg/) gehen.

