---
title: 'Events'
menu:
  main:
    parent: service
---

Im Studium gibt es viel mehr zu sehen und zu tun, als im Hörsaal zu hocken und der Vorlesung zuzuhören. Die Fachschaft ETIT organisiert deshalb immer wieder verschiedene Events, um euch Abwechslung zu bieten. Egal ob Partys oder Workshops, für jede:n ist was dabei.

## OPhest
Das OPhest ist das Highlight jeder OPhase. Es entsteht durch die Zusammenarbeit mit anderen Fachschaften. Es gibt gute Musik & leckere Drinks bei denen ihr mit euren neuen Kommiliton:innen richtig feiern könnt.

## Sommerfest
Jeden Sommer veranstaltet die Fachschaft ETIT ein Sommerfest, zu dem ihr alle herzlich eingeladen seid um mit uns zu feiern.

## Meet the Fachschaft
Du hast schon viel über die Fachschaft gehört und wunderst dich, was das für Menschen sind, die dir Klausuren verkaufen und immer für studienrelevante Fragen da sind?

Jedes Wintersemester laden wir euch auf einen Glühwein zum Gespräch mit uns ein, also kommt vorbei und lernt uns kennen.

## Wege ins Ausland
Du spielst schon länger mit dem Gedanken ins Ausland zu gehen?

Es gibt viele Möglichkeiten dafür und wir zeigen euch wie ihr am besten eure Studienleistungen im Ausland anerkennen lassen könnt und was es für Möglichkeiten für euch gibt.

## Lötkurs
Nur die graue Theorie über Schaltungen ist euch zu öde?

Bei dem Lötkurs bieten wir euch die Möglichkeit selbst den Lötkolben in die Hand zu nehmen und Platinen zu löten. Die Fachschaft steht euch dabei mit Tipps und Ratschlägen zur Seite.

## Vertiefungsrichtungsabend
Der Master rückt immer näher und es gibt viele Vertiefungsrichtungen.

Bei diesem Infoabend zeigen wir euch die verschiedenen Vertiefungen und deren Inhalte um euch die Auswahl zu erleichtern.

## Exkursion
In und um Karlsruhe gibt es sehr viele elektrotechnische Unternehmen, die wir gerne mit euch zusammen näher kennlernen wollen. Dazu besuchen wir die Werke und Standorte für spannende Führungen und interessante Vorträge.


