---
title: 'Events'
menu:
  main:
    parent: service
---

There is much more to see and do during your studies than just sitting in the lecture hall and listening to lectures. Therefore, the student council ETIT organizes various events to offer you some alternatives. Whether parties or workshops, there is something for everyone.

## OPhest
The OPhest is the highlight of every OPhase. It is created through collaboration with other student councils. There is good music & delicious drinks where you can celebrate with your new fellow students.

## Summer party
Every summer the student council of ETIT organizes a summer party to which you are all invited to celebrate with us.

## Meet the student council
You've heard a lot about the student council and wonder what kind of people they are who sell you exams and are always there for study-related questions?

Every winter semester we invite you for a mulled wine to talk to us, so come by and get to know us.

## Ways to go abroad
Have you been thinking about going abroad for a while?

There are many possibilities and we will show you the best way to get your studies abroad recognized and what opportunities there are for you.

## Soldering course
Only the gray theory about circuits is too boring for you?

At the soldering course we offer you the possibility to take the soldering iron in your hand and to solder circuit boards. The student council will help you with tips and advice.

## Master Specializations info event
The Master is getting closer and closer and there are many specializations.

At this information evening we will show you the different specializations and their contents to make your choice easier.

## Field Trips
In and around Karlsruhe there are many electro-technical companies, which we would like to get to know together with you. Therefore we visit the plants and locations for exciting guided tours and interesting lectures.

