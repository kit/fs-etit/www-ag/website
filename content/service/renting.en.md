---
title: 'Renting'
menu:
  main:
    parent: service
---

In the student council you can borrow various items and equipment for a deposit. Other student councils (Fachschaften) do not have to pay this deposit.

Just send an e-mail with the items you want to borrow and the period of time for which you need the items to [verleih@fs-etit.kit.edu](mailto:verleih@fs-etit.kit.edu). The rental must be confirmed at the Student Council meeting, which is held every Wednesday at 6pm. Therefore, if you need something on short notice, it is best to attend the meeting in person.

What we have for rent can be found in the [AStA-Wiki](https://wiki.asta-kit.de/verleih:start#fachschaft_etit).
