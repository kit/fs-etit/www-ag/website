---
title: "ETIT- and MIT-Lerning café winter semester 2024/2025"
date: 2025-02-12
thumbnail: news/2025_02_12_lerncafe.jpg
---

The ETIT faculty is once again organising its learning café this semester. 
The idea behind this is that it should be a place to learn together and find study groups. During the opening hours tutors, exercise instructors or professors of the relevant subjects are also on hand to help with questions and problems.

The learning café takes place in the LTI foyer (building 30.34).

You can find the opening hours and which subjects are offered on the [faculty website](https://www.etit.kit.edu/lerncafe.php).