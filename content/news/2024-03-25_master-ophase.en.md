---
title: "Master Orientation Summer 2024"
date: 2024-03-25
---

You are starting your Master's degree at KIT this Summer Semester? You are warmly invited to join our Master O-Phase from **10.04. - 17.04.**! You can expect a broad variety of events that you should not miss and that will be the best start into your Master at KIT that you can possibly have.

<!--more-->

All further infos can be found at the [website of the orientation week](https://o-phase.fs-etit.kit.edu/master).

If you have questions, feel free to reach out to us via [e-mail](mailto:o-phase@fs-etit.kit.edu).