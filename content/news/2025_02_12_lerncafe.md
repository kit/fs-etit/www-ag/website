---
title: "ETIT- und MIT-Lerncafé Wintersemester 2024/2025"
date: 2025-02-12
thumbnail: news/2025_02_12_lerncafe.jpg
---

Die Fakultät ETIT veranstaltet auch dieses Semester wieder ihr Lerncafé. 
Die Idee dahinter ist, dass dieses ein Ort sein soll um gemeinsam zu lernen und Lerngruppen zu finden. Während der entsprechenden Zeiten sind auch Tutor:innen, Übungsleier:innen oder Professor:innen der entsprechenden Fächer vor Ort um bei Fragen und Problemen zur Seite zu stehen.

Das Lerncafé findet im LTI-Foyer (Gebäude 30.34) statt.

Die Zeiten und welche Fächer angeboten werden findet ihr auf der [Website der Fakultät](https://www.etit.kit.edu/lerncafe.php).