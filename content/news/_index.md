---
title: Aktuelles
header: news/header.jpg
menu:
    main:
        identifier: news
        weight: 1
---

Auf dieser Seite findest du die neusten Informationen der Fachschaft Elektro- und Informationstechnik.
