---
title: "Vertiefungsabend 03.07 17:30 Uhr"
date: 2024-06-19
---

Ihr seid im zweiten Semester ETIT und wollt euch über die Vertiefungsrichtungen im Bachelor informieren? 

ODER 

Ihr seid kurz vor dem Abschluss eures Studiums und sucht noch nach einer Abschlussarbeit? 

ODER  

Ihr wollt euch über die Vertiefungsrichtungen im Elektrotechnik Master informieren? 

 

Dann kommt am Mittwoch, den 03.07.2024 ab 17.30 Uhr, zum VDE Vertiefungsabend im Audimax (Geb. 30.95). Hier werdet Ihr die Möglichkeit haben in persönlichen Gesprächen mit den Professorinnen und Professoren, Fachstudienberatenden und Promovierenden der einzelnen Institute der Fakultät Elektro- und Informationstechnik kennenzulernen und einen Überblick über die verschiedenen Vertiefungsrichtungen und Abschlussarbeiten der Institute zu bekommen. 

Darüber hinaus wird ab 18 Uhr für die Studierenden des zweiten Semesters im Audimax Hörsaal erklärt, wie die Wahl einer Vertiefungsrichtungen im Bachelor funktioniert und diese mit einem Überblick über die Themen vorgestellt. 