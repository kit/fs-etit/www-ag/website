---
title: "Infoveranstaltungen der Fakultät bezüglich der neuen Masterstudiengänge"
Date: 2024-11-07
---

Verbesserung: der Infoevent bezüglich des neuen Master Electrical Engeneering and Information Technologies findet am 26.11. statt.

Zum nächsten Wintersemester soll der Masterstudiengang Biomedical Engeneering in unserer Fakultät anlaufen. Wer Medizintechnik im Bachelor studiert, oder sich in seinem Elektrotechnik oder Mechatronik Bachelor für Medizintechnik interessiert sollte auf jeden Fall vorbeikommen. Die Infoveranstaltung soll am 19.11. um 15.45 Uhr im Hochspannungshöraal (HSI) stattfinden.
Wichtig: Der Master Biomedical Engeneering wird auf Englisch sein. Es wird im neuen Master Elektrotechnik keine Vertiefungsrichtung Medizintechnik geben.

Zum nächsten Semester wird der Studiengang Elektro- und Informationstechnik Master eine neue Studienprüfungsordnung erhalten. In dieser wird der Studiengang grundsätzlich überarbeitet, besonders im Aufbau der Vertiefungsrichtungen. Außerdem wird er vollkommen auf Englisch sein.
Das Infoevent zum neuen Master Elektrotechnik wird am 26.11. um 15.45 Uhr im Hochspannungshörsaal (HSI) stattfinden.