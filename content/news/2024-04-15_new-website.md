---
title: "Wilkommen auf unserer neuen Webseite"
date: 2024-04-15
thumbnail: news/corgis/corgi.png
---

Die Fachschaft ETIT ist jetzt mit neuer Webseite auf der Datenautobahn!

Hier findet ihr aktuelle Informationen zu eurem Studium, sowie zu den Services die wir euch bieten. Und davon gibt's ne ganze Menge!

<!--more-->

Auf der neuen Webseite findest du nicht nur aktuelle Informationen zu ihrem Studium, sondern auch zu den vielfältigen Services, die wir anbieten.

Von Öffnungszeiten für Beratungen und Altklausurverkauf, über regelmäßige Events und Exkursionen bis hin zur Veröffentlichung der Fachschaftszeitung "Der Funke" - wir begleiten euch durch das gesamte Studium. Vom Studieninteresse, über Freizeitbeschäftigung, bis hin zum Abschluss.

Schau dich doch gerne ein wenig um. Bei weiteren Fragen oder Ideen, kannst du gerne während einer [Öffnungszeit](/service/oeffnungszeiten) vorbeikommen!