---
title: "Information event for ETIT/MEDT/MIT-Studierstudentsnde in the 3rd semester"
date: 2025-02-12
thumbnail: news/2025_02_12_InfoeventDritties.jpg
---

The faculty invites all students in their third semester to an information event about their further studies. 

The topics will revolve around internships, Bachelor's theses, semesters abroad and the transition from Bachelor's to Master's degree programmes.

The event will take place on Monday 17 February 2025 in the Engelbert Arnold lecture theatre (Building 11.10) from 9:45 am to 11:15 am. Afterwards there will be doughnuts to digest the information.