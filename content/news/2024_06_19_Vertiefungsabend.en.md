---
title: "In-depth evening 03.07 17:30"
date: 2024-06-19
---

Are you in your second semester of ETIT and want to find out about the specializations in the Bachelor's degree? 

OR 

You are about to finish your studies and are still looking for a thesis? 

OR  

Would you like to find out about the specializations in the Electrical Engineering Master's program? 

 

Then come to the VDE specialization evening on Wednesday, 03.07.2024 from 17.30 in the Audimax (Building 30.95). Here you will have the opportunity to get to know the professors, student advisors and doctoral students of the individual institutes of the Faculty of Electrical Engineering and Information Technology in personal discussions and to get an overview of the various specializations and theses of the institutes. 

In addition, from 6 p.m. in the Audimax lecture hall, second semester students will be given an explanation of how to choose a specialization in the Bachelor's degree and an overview of the topics. 

Translated with DeepL.com (free version)