---
title: News
header: news/header.jpg
menu:
    main:
        identifier: news
        weight: 1
---

On this page you will find the latest information from the student council Electrical Engineering and Information Technology.
