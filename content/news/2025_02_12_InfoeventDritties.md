---
title: " Infoveranstaltung für ETIT/MEDT/MIT-Studierende ab dem 3. Semester"
date: 2025-02-12
thumbnail: news/2025_02_12_InfoeventDritties.jpg
---

Die Fakultät lädt alle Studierenden im dritten Semester zu einer Infoveranstaltung über ihr weiteres Studium ein. 

Die Themen drehen sich um Praktika, Bachelorarbeiten, Auslandssemester und den Übergang vom Bachelor in den Master.

Das Event findet am Montag dem 17.02.2025 im Engelbert-Arnold-Hörsaal (Gebäude 11.10) von 9:45 Uhr bis 11:15 Uhr statt. Im Anschluss wird es zum Verdauen der Informationen Berliner geben.