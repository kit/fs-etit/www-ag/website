---
title: "Welcome to our new website!"
date: 2024-04-15
thumbnail: news/corgis/corgi.png
---

The ETIT student council is now on the information superhighway with a new website!

Here you can find up-to-date information about your studies and the services we offer. And there are a lot of them!

<!--more-->

On the new website you will not only find up-to-date information about your studies, but also about the wide range of services we offer.

From opening hours for consultations and old exam sales, to regular events and excursions, to the publication of the student council newspaper "Der Funke" - we accompany you throughout your studies. From your interest in studying, to leisure activities, to graduation.

Take a look around. If you have any further questions or ideas, you are welcome to drop by during [opening hours](/service/opening-hours)!
