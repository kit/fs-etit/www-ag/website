---
title: "Infoveranstaltung O-Phasen Orga"
date: 2024-04-30
thumbnail: news/2024_05_ophase_infoveranstaltung.jpeg
---
Ihr hattet viel Spaß bei der letzten Ophase und habt Lust bei der Organisation der nächsten mitzumachen?

Dann komm zu unserer Infoveranstaltung!

Wir laden Dich herzlich zu unserer Informationsveranstaltung ein, bei der wir alles Wichtige zur Organisation der Orientierungsphase besprechen. Lerne das aktuelle Team kennen, erfahre mehr über die Aufgaben eines Organisators und wie Du Deine Ideen einbringen kannst.

**Wo und Wann?**  
Dienstag 7.Mai 18Uhr  kleiner ETI Hörsaal im ETI Geb: 11.10 

Falls du an disem Termin keine Zeit hast, aber trotzdem Lust hast eine OPhase zu organisieren, dann melde dich einfach bei:  
[o-phase@fs-etit.kit.edu](mailto:o-phase.fs-etit.kit.edu)
