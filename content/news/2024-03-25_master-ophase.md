---
title: "Master-O-Phase Sommer 2024"
date: 2024-03-25
---

Du beginnst in diesem Sommersemester dein Masterstudium am KIT? Dann bist du herzlich eingeladen, an unserer Master O-Phase vom **10.04. - 17.04.** teilzunehmen! 

Es erwartet dich ein breites Spektrum an Veranstaltungen, die du nicht verpassen solltest und die dir den besten Start in deinen Master am KIT ermöglichen werden.

<!--more-->

Weitere Informationen zur Master-O-Phase findest du auf der [Webseite der O-Phase](https://o-phase.fs-etit.kit.edu/master).

Solltest noch Fragen offen sein, kannst du uns per [E-Mail](mailto:o-phase@fs-etit.kit.edu) erreichen.