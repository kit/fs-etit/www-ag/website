---
title: "Bachelor O-Phase"
date: 2024-08-14
---

Solltest du im kommenden Semester in deinen neuen Lebensabschnitt dem Studium starten, dann bist du hier genau an der richtigen Stelle. Wenn du vor hast dich optimal auf das Studium vorzubereiten, dann kannst dich hier auf unserer Webseite über die Orientierungsphase unter [Webseite der Bachelor O-Phase](https://bestefachschaftderwelt.de/studies/ophase/) informieren. In der Orientierungsphase, kurz O-Phase, lernst du deine Mitstudierenden kennen und wie das Studium und das KIT im Großen und Ganzen funktionieren.

Möchtest du dich als Helfer in der O-Phase engagieren, dann kannst du dich gerne unter dem folgenden Link anmelden: [Tutorenanmeldung](https://survey.fs-etit.kit.edu/252681?lang=de-informal). Weitere Infos für Tutoren findest du unter - [Infos für Tutoren](/infos_fuer_tutoren.pdf)
