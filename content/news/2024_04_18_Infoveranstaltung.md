---
title: "Infoveranstaltung 4.Semester 24.04.2024"
date: 2024-04-18
---
Ihr studiert gerade im 4. oder 6. Semester? Dann seid ihr herzlich eingeladen zu unserer Infoveranstaltung am 24.April.

Informieren möchten wir euch zu folgenden Themen:
- Was muss ich bei einem Praktikum beachten?
- Welche Auswirkungen hat die neue SPO für mein Studium?
- Welche organisatorischen Dinge kommen bei einer Bachelorarbeit auf mich zu?
- Wie plane ich ein Auslandssemester?
- Übergang Bachelor à Master: Wie verläuft dieser reibungslos ab?
 
Wann? Montag, 24.April 2024, 17:30 Uhr – ca. 19:00 Uhr
 
Wo?      10.50 Bauingenieure Großer Hörsaal
