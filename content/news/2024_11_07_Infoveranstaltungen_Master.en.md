---
title: "Faculty's Info Event about new Masters Electrical Engeneering and Biomedical Engeneering"
Date: 2024-11-07
---

Correction: the Infoevent about the new Masters course Electrical Engeneering and Information Technologies will take place on the 26th of November.

Next Winter Semester the Faculty Electrical Engeneering will launch its new master course Biomedical Engeneering. Everyone ether studying "Medizintechnik" or Electrical / Mechatronics Engeneering and who is interested in studying Biomedical Engeneering as their masters should definitly attend. The event will take place at High-Voltage-Lecture Hall ("Hochspannungshörsaal") on the 19th of November  at 3:45 pm. It should be noted, that Biomedical Engeneering, in Contrast to "Medizintechnik" will be in english and that there will be no Medical Engeneering Field of Specialization in the new master courses Electrical Engeneering and Mecatronics Engeneering.

Next Semester, the updated master course Electrical Engeneering will be launched at our faculty. The master course will be completely remastered and fundamentally changed, especially in relation to the fields of specialization. The new master course will be in english.
The info event will take place at the High-Voltage-Lecture Hall ("Hochspannungshörsaal") on the 26th of November at 3:45 pm.
