---
title: Contact
menu: main
---

Welcome to the contact page of the student council Electrical Engineering and Information Technology at the Karlsruhe Institute of Technology.

## Premises and opening hours

### Postal address
```
Fachschaft Elektro- und Informationstechnik
Karlsruhe Institute of Technology (KIT)
Gebäude 11.10 (ETI)
Kaiserstraße 12
76131 Karlsruhe
```

### Visitor address
```
Fachschaft Elektro- und Informationstechnik
Gebäude 11.10 (ETI)
Engelbert-Arnold-Strasse 5
76131 Karlsruhe
```

### Electronic
```
Mail: info@fs-etit.kit.edu
Phone: +49 721 608-43783
Fax: +49 721 608-943783
```

Our **office** is located in building 11.10, Engelbert-Arnold-Straße 5, in the Electrotechnical Institute (ETI) of the Karlsruhe Institute of Technology.

We have noted our regular **opening hours** in the calendar. You can still visit us at any time. There is a good chance that you will meet someone in the student council rooms during lecture hours!

## Contact persons

{{< highlight >}}
**CAUTION:** We do not forward <u>any surveys</u> as a matter of principle and without exception!
{{</ highlight >}}


The work of the student council is divided into different **departments**. For special inquiries please contact the corresponding department.

This way you help us to answer your request directly and concretely and relieve our main mailing list info@fs-etit.kit.edu.

To make it easier for you to choose the best email address, here is a short table. 

{{< unsafe >}}
<table class="table">
    <thead>
        <th>Topic</th>
        <th>Contact Person</th>
    </thead>
    <tbody>
        <tr>
            <td>Questions about your studies?</td>
            <td rowspan="4" class="align-middle"><a href="mailto:info@fs-etit.kit.edu">info@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Questions about the study situation?</td>
        </tr>
        <tr>
            <td>Information about events?</td>
        </tr>
        <tr>
            <td>General information and advice</td>
        </tr>
        <tr>
            <td>Questions about the orientation phase</td>
            <td><a href="mailto:o-phase@fs-etit.kit.edu">o-phase@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Questions about student council parties</td>
            <td><a href="mailto:kultur@fs-etit.kit.edu">kultur@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Criticism or suggestions for improving the course?</td>
            <td rowspan="2" class="align-middle"><a href="mailto:skom@fs-etit.kit.edu">skom@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Complaints or praise for lecturers?</td>
        </tr>
        <tr>
            <td>Failed the oral re-exam?</td>
            <td rowspan="4" class="align-middle"><b>Bachelor:</b> <a href="mailto:bpa@fs-etit.kit.edu">bpa@fs-etit.kit.edu</a><br/><b>Master:</b> <a href="mailto:mpa@fs-etit.kit.edu">mpa@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Missed the deadline for retaking an exam?</td>
        </tr>
        <tr>
            <td>Can't complete your Bachelor's degree in ten semesters?</td>
        </tr>
        <tr>
            <td>Questions for the examination board?</td>
        </tr>
        <tr>
            <td>Questions about industrial excursions, company contacts</td>
            <td><a href="mailto:indu@fs-etit.kit.edu">indu@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Errors in exam protocols or collections of exam papers</td>
            <td rowspan="2" class="align-middle"><a href="mailto:klausuren@fs-etit.kit.edu">klausuren@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Do you have a protocol, notes, or a catalog of exam questions?</td>
        </tr>
        <tr>
            <td>Errors on the website</td>
            <td rowspan="2" class="align-middle"><a href="mailto:wwwag@fs-etit.kit.edu">wwwag@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Dead link</td>
        </tr>
    </tbody>
</table>
{{</ unsafe >}}


**But even if your problem is not directly in the table, don't be afraid to just write an email to info@fs-etit.kit.edu!**
