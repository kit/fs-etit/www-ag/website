---
title: Kontakt
menu: main
---

Willkommen zur Kontaktseite der Fachschaft Elektro- und Informationstechnik am Karlsruher Institut für Technologie.

## Räumlichkeiten und Öffnungszeiten

### Postanschrift
```
Fachschaft Elektro- und Informationstechnik
Karlsruher Institut für Technologie (KIT)
Gebäude 11.10 (ETI)
Kaiserstraße 12
76131 Karlsruhe
```

### Besucheradresse
```
Fachschaft Elektro- und Informationstechnik
Gebäude 11.10 (ETI)
Engelbert-Arnold-Straße 5
76131 Karlsruhe
```

### Elektronisch
```
Mail: info@fs-etit.kit.edu
Telefon: +49 721 608-43783
Fax: +49 721 608-943783
```

Unsere **Fachschaftsräume** befinden sich im Gebäude 11.10, in der Engelbert-Arnold-Straße 5, im Elektrotechnischen Institut (ETI) des Karlsruher Instituts für Technologie.

Wir haben unsere reguläre **Öffnungszeiten** im Kalender vermerkt. Du kannst uns trotzdem jeder Zeit besuchen. Es gibt gute Chancen, dass du jemanden in den Fachschaftsräumlichkeiten während der Vorlesungszeit triffst!

## Ansprechspartner

{{< highlight >}}
**ACHTUNG:** Wir leiten grundsätzlich und ausnahmslos <u>keine Umfragen</u> weiter!
{{</ highlight >}}


Die Arbeit der Fachschaft ist in verschiedene **Referate** aufgeteilt. Für spezielle Anfragen wende Dich doch bitte an das entsprechende Referat.

So hilfst Du uns, deine Anfrage direkt und konkret zu beantworten und entlastest unseren Hauptverteiler info@fs-etit.kit.edu.

Um euch die Wahl der besten Emailadresse zu erleichtern, eine kurze Tabelle. 

{{< unsafe >}}
<table class="table">
    <thead>
        <th>Thema</th>
        <th>Ansprechpartner</th>
    </thead>
    <tbody>
        <tr>
            <td>Fragen zum Studium?</td>
            <td rowspan="4" class="align-middle"><a href="mailto:info@fs-etit.kit.edu">info@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Fragen zur Studiensituation?</td>
        </tr>
        <tr>
            <td>Hinweis auf Veranstaltungen?</td>
        </tr>
        <tr>
            <td>Allgemeine Infos und Beratungen</td>
        </tr>
        <tr>
            <td>Fragen zur Orientierungsphase</td>
            <td><a href="mailto:o-phase@fs-etit.kit.edu">o-phase@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Fragen zu Fachschaftsfesten</td>
            <td><a href="mailto:kultur@fs-etit.kit.edu">kultur@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Kritik oder Verbesserungsvorschläge am Studiengang?</td>
            <td rowspan="2" class="align-middle"><a href="mailto:skom@fs-etit.kit.edu">skom@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Beschwerden oder Lob über Dozenten?</td>
        </tr>
        <tr>
            <td>Die mündliche Nachprüfung nicht bestanden?</td>
            <td rowspan="4" class="align-middle"><b>Bachelor:</b> <a href="mailto:bpa@fs-etit.kit.edu">bpa@fs-etit.kit.edu</a><br/><b>Master:</b> <a href="mailto:mpa@fs-etit.kit.edu">mpa@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Frist für die Wiederholung einer Prüfung verpasst?</td>
        </tr>
        <tr>
            <td>Bachelor nicht in zehn Semestern möglich?</td>
        </tr>
        <tr>
            <td>Fragen an den Prüfungsausschuss?</td>
        </tr>
        <tr>
            <td>Fragen zu Industrieexkursionen, Firmenkontakten</td>
            <td><a href="mailto:indu@fs-etit.kit.edu">indu@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Fehler in Prüfungsprotokollen oder Klausursammlungen</td>
            <td rowspan="2" class="align-middle"><a href="mailto:klausuren@fs-etit.kit.edu">klausuren@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Du hast ein Protokoll, einen Mitschrieb oder einen Prüfungsfragenkatalog?</td>
        </tr>
        <tr>
            <td>Fehler auf der Website</td>
            <td rowspan="2" class="align-middle"><a href="mailto:wwwag@fs-etit.kit.edu">wwwag@fs-etit.kit.edu</a></td>
        </tr>
        <tr>
            <td>Link tot</td>
        </tr>
    </tbody>
</table>
{{</ unsafe >}}

**Aber auch, wenn dein Problem nicht direkt in der Tabelle steht, scheue nicht, einfach eine Email an info@fs-etit.kit.edu zu schreiben!**
