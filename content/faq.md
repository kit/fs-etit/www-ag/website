---
title: FAQ
menu: main
weight: 6
showDate: true
---

{{< accordion "faq" >}}

{{% accordion-item "Ich muss einen Antrag zur Fristverlängerung/Prüfungswiederholung stellen, was mache ich nun?" %}}
Wir haben für euch ein paar Tipps verfasst: 
- Gründlich die Studienordnung lesen. Hier findet man alle wichtigen Informationen zu Maximalstudiendauer, Prüfungsregelungen und co.
- Mit den BPA/ MPA- Zuständigen in der Fachschaft reden. Diese können eure Fragen beantworten und Tipps geben, wie man einen formalen Antrag an den Prüfungsausschuss richtig stellt.
- Ihr könnt euch auch direkt an die Prüfungsausschussreferentinnen wenden, um euch über das Antragsverfahren zu informieren. Der Antrag muss am Ende auch bei ihnen im Sekretariat eingereicht werden.
- Ruhig bleiben. Es ist nicht das Ende der Welt. Ihr seid nicht die ersten und werdet auch nicht die letzten sein, die in dieser Situation sind. 
{{%/ accordion-item %}}

{{% accordion-item "Ich habe Probleme bei der Online-Prüfungsanmeldung. Wer kann mir helfen?" %}}
Melde dich beim Studiengangsservice der Fakultät. Die können dich im Campus System für die gewünschte Klausur anmelden. 
- Email: [Bachelor Service](mailto:bachelor-info@etit.kit.edu), [Master Service](mailto:master-info@etit.kit.edu) 
- Websiten: [Bachelor Studiengangsservice](https://www.etit.kit.edu/studiengangservice_bachelor_etit_medt_mit.php), [Master Studiengangsservice](https://www.etit.kit.edu/studiengangservice_master_etit_und_mit.php) 
{{%/ accordion-item %}}

{{% accordion-item "Gibt es auch eine O-Phase für externe Masterstudierenden?" %}}
Ja, auch für neue Masterand:innen gibt es eine Orientierungswoche. Die sogenannte Mo-phase ist in der Regel kleiner als der Bachelor O-Phase. Das Programm und weitere wichtige Informationen findest du auf der [O-Phasen Website](https://o-phase.fs-etit.kit.edu).
{{%/ accordion-item %}}

{{% accordion-item "Was sind Schlüsselqualifikationen und in welchem Semester mache ich sie am besten?" %}}
Du musst im Laufe deines Bachelors und Masters ein paar ECTS durch sogenannte Schlüsselqualifikationen erlangen. Wie viele ECTS du für deinen Abschluss brauchst, ist in der Prüfungsordnung geregelt. Zu Schlüsselqualifikationen gehören z. B. „Soft Skills“, Sprachen oder Lerntechniken.

Sie sind für kein bestimmtes Semester vorgeschrieben, es ist aber empfehlenswert, sich früh darum zu kümmern – nicht direkt im ersten Semester, aber auch nicht erst während der Bachelorarbeit.

Es gibt am HoC, ZAK und Sprachzentrum viele interessante Angebote, von denen man einiges mitnehmen kann. Aber auch im Modulhandbuch findet man einige Schlüsselqualifikationen, welche direkt von der Fakultät angeboten werden.
{{%/ accordion-item %}}

{{% accordion-item "Wie läuft die Vergebung der Tutorien? Kann es zu Kollisionen kommen?" %}}
Bei den ETITs und MEDTs erfolgt die Vergebung der Zeiten meistens über das WiWi-Portal oder das neue Campus Plus Portal.

Da kannst du alle Tutorienzeiten von 1 bis 5 Sterne einstufen, damit du keine ungünstige (1 Stern) Zeiten bekommst. Mehr Informationen bekommst du dort. Innerhalb eines Portals kann es nicht zu Kollisionen kommen.

Bei MIT kommt noch das Portal Redseat dazu. Leider kam es in den letzten Jahren zu Kollisionen zwischen Redseat und anderen Portalen. Sollte dir das passieren, sind leider die Zeiten der TM-Tutorien und MKL-Workshop nicht verhandelbar. Bei den anderen Tutorien kannst du ein neues finden, indem du einfach zu Zeit und Ort des Tutoriums vorbei kommst und fragst, ob noch Plätze vorhanden sind. 
{{%/ accordion-item %}}

{{% accordion-item "Wie kann ich mich über das Studium am KIT informieren?" %}}
Jedes Jahr im November findet der Studieninformationstag statt, an dem du viele interessante Infos erhältst und sogar Schnuppervorlesungen besuchen kannst. Schüler:innen aus Baden-Würrtemberg werden hierfür auch von ihrer Schule freigestellt. Zusätzlich findet jedes Jahr an einem Samstag im Mai der Campustag statt. Auch hier gibt es viele Events und Infostände über den Campus verteilt wo man sich über das KIT und einem Studium am KIT informieren kann.
Außerdem kannst du immer gerne einen Spaziergang über den Campus machen und in der Fachschaft vorbeischauen. Im Sommer führen wir öfters Campusführungen für Studieninteressierte durch.

Wenn du Interesse hast, an einer teilzuhaben, kannst du dich gerne per Mail melden. Wir freuen uns, dich kennen zu lernen.

Wenn du Beratung zu den verschiedenen Studiengängen am KIT suchst, kannst du auch zur [Zentralen Studienberatung](https://www.sle.kit.edu/wirueberuns/zsb.php) gehen. Die Büros sind im KIT Präsidium direkt gegenüber von unserer Fachschaft. 
{{%/ accordion-item %}}


{{% accordion-item "Woher bekomme ich Skripte für die Vorlesungen?" %}}
Die meisten kannst du auf den Vorlesungshomepages bzw. im ILIAS finden und dir beispielsweise im SCC selbst ausdrucken.

Bei den MITler ist der Skriptenverkauf in der Mensa interessanter, da dort die HMI-III und TMI-II Skripte verkauft werden. 
{{%/ accordion-item %}}



{{% accordion-item "Welchen Laptop/Taschenrechner soll ich mir kaufen?" %}}
**ETIT & MEDT:** Zu erst einmal gibt es da keine Vorgabe, die meisten haben ein Lenovo ThinkPad (auch wegen der Vergünstigungen für Studierende, sowie der Verfügbarkeit von gebrauchten Geräten). Ein günstiger Laptop stößt beim Programmieren und bei rechenaufwändigen Simulationen schnell an seine Grenzen. Diese Grenzen erreicht man in den Bachelorvorlesungen in der Regel jedoch nicht, außerdem [bietet das Rechenzentrum ](https://www.scc.kit.edu/dienste/pools.php) Arbeitsplätze, welche während der Öffnungszeiten nutzbar sind. Ein guter Akku ist jedoch nötig, da viele Lernplätze, sowie die Vorlesungssäle leider nicht so viele Steckdosen haben.
Am besten schaust du dir mal die Systemanforderungen für das Programm MatLab an.

**MIT:** Hierbei musst du bedenken, ob du die Vertiefungsrichtung MKL3/4 machen möchtest. Wenn dies der Fall ist, dann schaue dir die Systemanforderungen für das CAD-Programm Creo. Es gibt auch viele Studierende, die Vorlesungsnotizen mit dem iPad oder Surface machen. Die Anschaffung von solchen Geräten ist auch kein muss. Es ist dir überlassen, ob du sie sinnvoll findest. 

Ansonsten gelten die gleichen Informationen wie für die ETITs und MEDTs.

**ETIT/MEDT/MIT:** Zum Thema Taschenrechner: Kauf dir bloß keinen grafikfähigen und programmierbaren, der ist in den Klausuren nämlich gar nicht zugelassen. Die meisten haben den Casio fx-991: ein einfacher Taschenrechner, der viele Konstanten eingespeichert hat und alles kann, was in Klausuren erlaubt ist.
Viele Student:innen nutzen inzwischen Tablet oder einen Laptop mit Touchscreen um in der Vorlesung Notizen zu machen. Da die Foliensätze von Vorlesungen i.d.R. vorher hochgeladen werden, kann man Notizen direkt auf den Folien machen.
{{%/ accordion-item %}}



{{% accordion-item "Welche Software kriege ich als Student:in kostenlos?" %}}
Erst einmal ziemlich viel. Das Meiste findet ihr im [SCC Softwareshop](https://www.scc.kit.edu/dienste/softwareshop.php), unter anderem Matlab, LabView und sämtliche Microsoft-Betriebssysteme.

Außerdem natürlich Open-Source Software ;-)
{{%/ accordion-item %}}



{{% accordion-item "Bietet ihr auch Exkursionen an?" %}}
Diverse Institute bieten Exkursionen an. Sie werden meistens in der Vorlesung angekündigt sowie auf ihre Homeseiten. 

Wenn wir Exkursionen anbieten, werden sie auch in der Vorlesung angekündigt über diese Website und unsere Social Media Kanäle bekannt gegeben. Anmelden muss man sich bei uns vor Ort während einer Öffnungszeit.
{{%/ accordion-item %}}


{{% accordion-item "Was muss ich tun, um das Studienfach zu wechseln?" %}}
Für deinen neuen Wunschstudiengang musst du dich genau wie beim ersten Mal ganz normal bewerben. Zusätzlich musst du einen schriftlichen Nachweis über eine auf den angestrebten Studiengang bezogene studienfachliche Beratung erbringen.

Falls du dir Fächer von deinem ersten Studienfach anrechnen lassen möchtest, solltest du das direkt mit den entsprechenden Dozent:innen abklären. Die Anrechnung muss innerhalb des 1. Semesters gemacht werden!
{{%/ accordion-item %}}



{{% accordion-item "Wo finde ich den Stundenplan und ab wann wird dieser aktualisiert?" %}}
Den Stundenplan für den Bachelorstudiengang ETIT & MEDT findest du [hier](/studies/timetables).

Den Stundenplan für den Bachelorstudiengang MIT findest du [hier](https://www.mach.kit.edu/vorlesungsverzeichnis.php).

Achte darauf, dass hier nur deine Pflichtfächer aufgelistet sind. Vertiefungsfächer findest du auf den jeweiligen Institutsseiten.
{{%/ accordion-item %}}



{{% accordion-item "Wann habt ihr offen? Wann kann ich Klausuren/Prüfungsprotokolle erwerben?" %}}
In der Vorlesungszeit bieten wir im Normalfall mindestens eine Öffnungszeit pro Tag an. In der vorlesungsfreien Zeit bemühen wir uns, möglichst viele Termine abzudecken. Das klappt leider nicht immer und wenn, dann auch eher spontan.

Grundsätzlich ist es aber so, dass sehr oft Fachschaftler:innen da sind, auch wenn keine Öffnungszeit eingetragen ist. Falls man sowieso gerade am Campus ist, kann man natürlich einfach vorbeischauen. Die sichere Methode ist ein kurzer Anruf unter [0721 / 608-43783](tel:+4972160843783). Wenn jemand da ist, kann logischerweise auch gefragt werden, wie lange jemand vor Ort ist.

Übrigens kannst du auch selbst mal eine Öffnungszeit (mit-)machen, wir freuen uns über jede helfende Hand. Das kann eigentlich jede:r und ist gar keine schwere Aufgabe. Frag doch einfach mal bei deinem nächsten Besuch nach!
{{%/ accordion-item %}}



{{% accordion-item "Warum gibt es die Protokolle/Klausuren nicht online?" %}}
[Wir hatten darüber mal einen ausführlichen Funke-Artikel dazu geschrieben.](/funke_161.pdf)
Kurz gesagt: aus Datenschutz Gründen, dürfen wir leider keine Protokolle und Klausuren digital verschicken. Viele Dozent:innen laden aber inzwischen Altklausuren im jeweiligen Ilias Kurs hoch. 

{{%/ accordion-item %}}




{{% accordion-item "Ich komme jetzt ins 3./5. Bachelor Semester. Wo kann ich mich über Wahlmöglichkeiten informieren?" %}}
**ETIT:** Derzeitig werden folgende Vertiefungsrichtungen im Bachelor angeboten:
- Allgemeine Elektrotechnik & Informationstechnik (AEI)
- Automatisierung, Robotik & Systems Engineering (ARS)
- Elektrische Energiesysteme und Elektromobilität (EEE)
- Informations- & Kommunikationstechnik (IKT)
- Mikroelektronik, Photonik und Quantentechnologien (MPQ)

Zusätzlich gibt es einen Wahlbereich der völlig frei von der Vertiefungsrichtung ist. Welche Module zur Verfügung stehen kann man im Modulhandbuch nachlesen.

Die Wahl der Vertiefungsrichtung im Bachelor hat keinen Einfluss auf die Wahl der Vertiefungsrichtung im Master. Es ist also absolut möglich, sich thematisch noch einmal umzuentscheiden wenn man in den Master wächselt.

**MEDT:** Im Modulhandbuch findest du eine Liste mit allen Wahlfächern die man im MEDT Bachelor wählen darf. Diese Wahl hat keinen Einfluss auf deinen darauf folgenden Master. Solltest du dir überlegen für deinen Master zu ETIT zu wechseln, ist es ggf. sinnvoll die Pflichtmodule aus ETIT als Wahlfach zu wählen, die nicht schon im MEDT Pflichtbereich vorhanden sind. 

**MIT:** Im MIT sind die Vertiefungsfächer in Wahlpflichtmodule und Ergänzungsmodule geteilt. Wie sie ausgewählt werden sollen und wann man sich Ergänzungsmodule anrechnen lassen kann, könnt ihr im [Modulhandbuch](https://www.sle.kit.edu/vorstudium/bachelor-mechatronik-informationstechnik_13982.php) unter "Vertiefung in der Mechatronik" finden.

Ansonsten könnt ihr die Wahlpflichtsmodulvorstellung im 2. Semester besuchen. Sie wird von dem BPA und Dekanen des interfakultativen Studiengangs Mechatronik organisiert und wird immer ein paar Wochen davor in der Vorlesung angekündigt. Wenn du diese Veranstaltung verpasst hast, kannst du dir die Folien unter Wahlpflichtmodule im Vertiefungsfach anschauen. 
{{%/ accordion-item %}}




{{% accordion-item "Wo kann ich mich über Organisatorisches informieren und mit Kommiliton:innen connecten?" %}}
Falls du Fragen zu Übungsblättern, dem (Nicht-)stattfinden von Vorlesungen oder der Hörsaalverteilung für die nächste Klausur hast, kannst du (sofern ein Account vorhanden ist) in die WhatsApp-, Signal- oder Telegram-Gruppe deines Jahrgangs eintreten.

Diese werden normalerweise von der Ophasen-Organisation administriert. Dort können sich deine Kommilitonen und du gegenseitig offene Fragen beantworten. 

Außerdem gibt es einen Discord Server der von ETIT Kommiliton:innen moderiert wird. Komm gerne auf uns zu, wenn du den Link für diesen Server haben möchtest.
{{%/ accordion-item %}}




{{% accordion-item "Welche Bücher könnt ihr zu dem Fach X empfehlen?" %}}
Pauschal kann man die Frage natürlich nicht beantworten. Grundsätzlich ist es aber eine gute Idee, wenn du dich auch mit zusätzlichen Materialien auf eine Prüfung vorbereiten willst, da die meisten offiziellen Skripte einen hohen wissenschaftlichen Anspruch haben und daher schwierig zu verstehen sind, wenn man das Thema noch nicht komplett verstanden hat.

Man sollte sich allerdings auch darüber im Klaren sein, dass offiziellen Materialien der Dozent:innen später die Grundlage der Klausur darstellen und du dich deshalb nicht komplett auf empfohlene Bücher verlassen solltest.

Die meisten Dozent:innen stellen in der ersten Vorlesung oder in der ersten Übung ihre Empfehlungen für zusätzliche Literatur vor. Du könntest deshalb im Foliensatz der Lehrveranstaltung nach der Literaturliste suchen. Falls du dort nichts findest, kannst du den Übungsleiter auch per Mail nach Buchempfehlungen fragen.
{{%/ accordion-item %}}




{{% accordion-item "Ich kam zu spät nach Karlsruhe und konnte deshalb weder an der O-Phase noch an Lehrveranstaltungen in den ersten Vorlesungswochen teilnehmen. Welche wichtigen Informationen habe ich verpasst?" %}}
In den ersten Vorlesungen werden meistens nur organisatorische Dinge wie Zugangspasswörter zu Lehrmaterialien oder Empfehlungen für zusätzliche Literatur besprochen.

Bei solchen Fragen wendest du dich am besten an die zuständigen Übungsleiter:innen. Deren E-Mailadresse findest du auf der jeweiligen Institutshomepage.

Aber auch die Fachschaft ist bei Fragen rund ums Studium der richtige Ansprechpartner. Wenn du Fragen zu verschiedenen Lehrveranstaltungen (z.B. dem Workshop ETIT oder der Matlab Aufgabe in LEN) hast, kannst du gerne einfach in der Fachschaft vorbeischauen und uns mit deinen Fragen löchern. Nach Terminabsprache können wir dir auch eine nachträgliche Campusführung anbieten, bei der wir dir verschiedene wichtige Einrichtungen auf dem Campus zeigen.
{{%/ accordion-item %}}

{{% accordion-item "Wann werden Prüfungsergebnisse veröffentlicht?" %}}
In der Regel werden die Prüfungsergebnisse innerhalb 4 bis 6 Wochen nach der Prüfung im Campus Portal veröffentlicht. Um direkt benachrichtigt zu werden, wenn eine neue Note veröffentlicht wird, kannst du im Campus Portal im Menü unter Persönliche Daten > Benachrichtigungen verschiedene E-Mail-Benachrichtigungen aktivieren.

Ab und zu kann es vorkommen, dass die Korrektur einer Prüfung wegen unvorhergesehener Umstände länger dauern könnte. Sollte die Note für den weiteren Studienfortschritt wichtig sein, kann man sich aber bei den Übungsleitenden melden, um nach einer "4.0-Bescheinigung" zu fragen. Diese bestätigt, dass man die Klausur mit mindestens 4.0 bestanden hat. Die richtige Note wird dann nach der Korrektur nachgetragen.

Sollte die Korrektur wesentlich länger als 6 Wochen dauern, könnt ihr euch gerne bei uns melden. 
{{%/ accordion-item %}}

{{% accordion-item "Ich bin in Karlsruhe, die O-Phase hat noch nicht begonnen und der Nachmittag nach dem Vorkurs ist lang. Was soll ich tun?" %}}
Wenn gutes Wetter ist, empfiehlt es sich immer in den Schlosspark zu gehen! Sportkurse sind leider gerade keine gute Anlaufstelle, weil du bei denen so kurz vor Semesterende nicht versichert bist.

Wenn du einfach in lockerer Atmosphäre Leute kennen lernen willst, dann schau doch mal im AKK oder Z10 vorbei.
{{%/ accordion-item %}}

{{% accordion-item "Wann sind Prüfungstermine für das übernächste Semester online?" %}}
In Zusammenarbeit mit dem BPA und MPA versuchen wir die Prüfungstermine so früh wie möglich zu planen. Sobald sie feststehen, findest du sie auf der Fakultätshomepage für den ETIT Bachelor, MEDT Bachelor und ETIT Master sowie im Schaukasten vor der Fachschaft. Alle MIT Prürungstermine findest du [hier](https://www.ak-mit.vs.kit.edu/docs/organisatorisches/#klausurtermine)
{{%/ accordion-item %}}

{{% accordion-item "Wann sind Ferien?" %}}
Die Weihnachtsferien am KIT sind vom 24.12 (Heiligabend) bis einschließlich 06.01 (Dreikönige). In dieser Zeit sollten keine Veranstaltungen stattfinden.
Je nachdem, auf welche Wochentage das fällt, können auch Veranstaltungen davor oder danach ausfallen. Das hängt dann von den Instituten bzw. den Veranstaltern selbst ab.
Im Sommersemester ist die Pfingstwoche (also die Woche die mit Pfingstmontag startet) frei. Einige Institute bieten in dieser Woche Exkursionen an, für die man sich anmelden kann. 
Die genauen Semester und Vorlesungsfreienzeiten findest du [hier](https://www.sle.kit.edu/imstudium/termine-fristen.php)

Im Übrigen gibt es vor Weihnachten auch Vorlesungen, die keinen prüfungsrelevanten Stoff behandeln.


{{%/ accordion-item %}}


{{% accordion-item "Ich habe die mündliche Nachprüfung nicht bestanden. Was passiert jetzt?" %}}
Das ist erstmal unschön, aber nichts, was andere nicht auch schon durchgemacht haben. Du hast zwar erstmal deinen Prüfungsanspruch für diese Veranstaltung verloren (und zwar nicht nur am KIT), aber es gibt noch Möglichkeiten, dass du dein Studium dennoch fortsetzen kannst.

Solche Fälle behandeln die Prüfungsausschüsse (Bachelor- und Masterprüfungsausschuss), in denen auch Studierende aus der Fachschaft sitzen. Am besten nimmst du direkt mit ihnen Kontakt auf um von Ihnen beraten zu werden. 

Die Kontaktadressen sind [bpa@fs-etit.kit.edu](mailto:bpa@fs-etit.kit.edu) für Angelegenheiten im Bachelorstudium und [mpa@fs-etit.kit.edu](mailto:mpa@fs-etit.kit.edu) im Masterstudium.

**Wichtig:** Bei Orientierungsprüfungen ist folgendes aus der Prüfungsordnung (Paragraph 8, Absatz 2) zu beachten: "Wer die Orientierungsprüfungen einschließlich etwaiger Wiederholungen bis zum Ende des
dritten Fachsemesters nicht erfolgreich abgelegt hat, verliert den Prüfungsanspruch im Studiengang, es sei denn, dass die Fristüberschreitung nicht selbst zu vertreten ist; hierüber entscheidet der Prüfungsausschuss auf Antrag der oder des Studierenden. Eine zweite Wiederholung der Orientierungsprüfungen ist ausgeschlossen."
{{%/ accordion-item %}}

{{% accordion-item "Ich habe eine Wiederholungsprüfung nicht bestanden. Was passiert jetzt?" %}}
Das ist erstmal nicht weiter schlimm, das passiert einigen von uns. Du musst nun eine mündliche Nachprüfung machen, die allerdings nicht besser als 4,0 bewertet werden kann. Zur besseren Vorbereitung kannst du dir mündliche Prüfungsprotokolle bei uns abholen. Es gibt Wiederholungsprüfungen, die tendenziell öfter stattfinden als andere. Daher haben wir von diesen dann mehr Protokolle als von anderen.

Da auch einige von uns Fachschaftlern das Prozedere selbst durchgemacht haben, scheue dich nicht bei uns vorbeizukommen und dir Erfahrungsberichte anzuhören.
{{%/ accordion-item %}}

{{% accordion-item "Ich habe eine Prüfung nicht bestanden. Was passiert jetzt?" %}}
Das ist erstmal nicht weiter schlimm, das passiert fast jedem von uns. Du kannst im kommenden Semester einfach die Nachprüfung schreiben oder im Semester danach die Vorlesung nochmal hören und dann nochmal schreiben. Für die Orientierungsprüfungen gelten allerdings ein paar Besonderheiten. Diese müssen bis zum Ende des 3. Fachsemesters bestanden sein. Ein Antrag auf Zweitwiederholung ist nicht möglich!
{{%/ accordion-item %}}




{{% accordion-item "Wann ist die O-Phase? Was erwartet mich?" %}}
Die Orientierungsphase findet in der Woche vor dem Vorlesungsbeginn statt, die Teilnahme ist zwar nicht Pflicht aber ratsam.

Dort wirst du auf dein Studium vorbereitet: Allerhand Informationen über das Studium selbst, das KIT und so weiter. Darüberhinaus lernst du den Campus, die Stadt, Abendaktivitäten und das Wichtigste - deine Kommilitonen kennen.
{{%/ accordion-item %}}



{{% accordion-item "Ich bin auf der Suche nach einem Praktikum, Abschlussarbeit, Job etc., habt ihr Stellenangebote?" %}}
Wir hängen regelmäßig Angebote/Gesuche in unseren Schaukästen bei der Fachschaft aus. Ausserdem gibt es für Interessierte den E-Mailverteiler [angebote@fs-etit.kit.edu](mailto:angebote@fs-etit.kit.edu), bei dem regelmäßig Gesuche eingehen. Nähere Informationen gibt es hier.

Plakate und Aushänge können uns direkt zugeschickt werden [indu@fs-etit.kit.edu](mailto:indu@fs-etit.kit.edu) oder im DIN A4-Format (bitte als PDF) an [innen@fs-etit.kit.edu](mailto:innen@fs-etit.kit.edu). Diese werden ausschließlich in schwarz-weiß gedruckt.
{{%/ accordion-item %}}



{{% accordion-item "Ich bin nicht in Karlsruhe und benötige Klausuren/Protokolle zu XY. Was kann ich tun?" %}}
In sehr gut begründeten Ausnahmefällen verschicken wir Klausuren und Protokolle auch per Post zu dir nach Hause. Neben den Druckkosten fallen dann auch noch Versandkosten an, die du entweder vor Ort oder per Überweisung bezahlen kannst.

Grundsätzlich können und dürfen wir Klausuren und Protokolle NICHT digital zu Verfügung stellen! 
{{%/ accordion-item %}}




{{% accordion-item "Welche Vorkurse soll ich vor dem Studium besuchen?" %}}
Der Vorkurs für den groben Überblick oder für die Auffrischung des Abistoffs Mathe findet in den beiden Wochen vor Semesterbeginn statt. Da die meisten Studerenden dort hingehen, kannst du vor allem dort viele neue Leute kennen lernen.

Wenn das Abitur schon etwas länger her ist, oder du Mathe und Physik in der Schule nicht intensiv behandelt hast, gibt es einige sehr ausführliche Vorkurse des MINT-Kollegs Baden-Württemberg. Für diese musst du dich anmelden.
{{%/ accordion-item %}}



{{% accordion-item "Habt ihr Protokolle zu XY?" %}}
Eine Liste verfügbarer Protokolle findest du [hier](/service/minutes/).

Falls zur Vorlesung kein Protokoll vorhanden ist, tut uns das sehr Leid - wir haben aber nur bedingt Einfluss auf das Angebot. Du kannst aber dein Wissen teilen, indem du den Protokollvordruck ausfüllst. Deine Kommilitonen werden es dir danken! Entweder du schickst das Protokoll dann an das [Klausurenreferat](mailto:klausuren@fs-etit.kit.edu) oder bringst es in der Fachschaft vorbei (es erwartet dich dann auch ein kleines Dankeschön).

Um Redundanz und Streuung zu vermeiden sollten die Protokolle zu Vorlesungen anderer Fakultäten (MACH/CIW, INFO, etc.) bei der entsprechenden Fachschaft erhältlich sein.
{{%/ accordion-item %}}

{{% accordion-item "Wo finde ich die Kurse für meinen Master in ETIT/MEDT/MIT?" %}}
Wir haben hierfür ein Dokument angelegt. Das findest du [hier](/How_to_ILIAS_and_CAS.pdf) (leider nur in Englisch verfügbar).
{{%/ accordion-item %}}

{{</ accordion >}}
