---
title: Allgemein
menu:
    main:
        parent: studies
        weight: 1
showDate: true
---

Auf dieser Seite findest du viele wichtige und relevante Informationen die generell das Studium und spezifisch das Studium bei uns an der Fakultät betreffen. Bei Fragan kann man sich auch immer per [Mail](mailto:info@fs-etit.kit.edu) an uns wenden.

## Studium
- [An- und Abmeldungen für Prüfungen](https://campus.studium.kit.edu/exams/index.php)
- {{< relurl "studies/timetables" >}}Stundenpläne{{</ relurl >}}
- [Rückmeldungen für das neue Semester](https://campus.studium.kit.edu/renewal/index.php)
- [Studienbescheinigungen (Immatrikulations-Bescheid, KVV, ...)](https://campus.studium.kit.edu/reports/studyreports.php)
- [Ausland und sonstige Ausflüge](https://www.intl.kit.edu/ostudent/index.php)
- [Praktikantenamt](https://www.etit.kit.edu/praktikantenamt.php)
- [BAföG](https://www.sw-ka.de/de/finanzen/bafoeg/)
- [WLAN](https://www.scc.kit.edu/dienste/wlan.php)
- [Skripte oder sonstiges Drucken](https://www.asta-kit.de/de/angebote/druckerei)
- [Freie Lernplätze](https://www.bibliothek.kit.edu/cms/freie-lernplaetze-karte.php)

## Prüfungstermine
- [ETIT-Bachelorprüfungen](https://www.etit.kit.edu/pruefungstermine_bachelor.php)
- [ETIT-Masterprüfungen](https://www.etit.kit.edu/pruefungstermine_master.php)


## MINT-Kolleg
- [Vor dem Studium](https://www.mint-kolleg.kit.edu/Studienvorbereitung.php)
- [Während dem Studium](https://www.mint-kolleg.kit.edu/Studienbegleitung.php)

## Zusatzqualifikationen
- [Sprachkurse](https://spz.kit.edu/253.php)
- [HoC](https://studium.hoc.kit.edu/index.php/anmeldeverfahren)
- [ZaK](https://zak.kit.edu/anmeldung.php)

## Freizeitangebote
- [Sportkurse](https://www.ifss.kit.edu/hochschulsport/sportangebot.php)
- [Hochschulgruppen](https://www.asta-kit.de/de/engagier-dich/hochschulgruppen/gefoerdert)

## Probleme
- [Klausur durchgefallen, Leistungen werden nicht angerechnet](https://www.etit.kit.edu/beratung.php)
- [Studium macht keinen Spaß mehr, du überlegst abzubrechen oder brauchst Hilfe dabei, doch weiterzumachen](https://www.sle.kit.edu/imstudium/zib.php)
- [Uni ist doof, WG ist doof, Leben ist doof, alles ist doof](https://www.sw-ka.de/de/beratung/psychologisch/psychotherapeutische_beratungsstelle_karlsruhe/)
- ["Ich verklag dich!" - und sonstige rechtliche Probleme](https://www.asta-kit.de/de/angebote/beratung/rechtsberatung)

