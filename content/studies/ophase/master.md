---
title: "Master O-Phase"
showDate: true
---

Die kommende Master O-Phase wird vom 14.04. bis zum 25.04.2025 mit verschiedenen Events an allen Tagen stattfinden. Einen Kalender mit der detaillierten Auflistung findest Du im Anschluss dieses Textes.

Im weiteren Verlauf wollen wir dir kurz erklären was bei den einzelnen Events auf dich wartet:

**Die Einführungsveranstaltung**

 - Die Fakultät und die Fachschaft stellen sich vor. Des Weiteren wird Dir erklärt, welche Events in der MO-Phase auf dich wartet. Die Veranstaltung findet im Engelbert-Arnold-Saal im ETI (Geb. 11.10) in der Engelbert-Arnold-Straße 5, 76131 Karlsruhe statt.

**Campus Tour**

 - "Ältere" Studierende zeigen Dir den Campus. Treffpunkt ist vor der Fachschaft, im ETI. (Geb. 11.10)

**Grillen**

 - Wir treffen uns auf dem LTI-Hof (Geb. 30.34) und lassen den Abend bei gemeinsamen Grillen ausklingen. Leider können wir aufgrund von Budget-Beschränkungen das Essen nicht kostenlos anbieten, bring also bitte ein bisschen Bargeld mit.

**Prof Frühstück**

 - Der Name ist Programm, ein Frühstück mit deinen Professoren im LTI Foyer.

**Fakten with Simon**

 - Wir zeigen Dir wie Du durch die Uni-IT navigierst, an welche Regularien Du und die Uni sich halten müssen und geben Dir alle Informationen, die du für ein erfolgreiches Studium benötigst. Engelbert-Arnold-Saal, Geb. 11.10.

**Kneipentour**

 - Eine Tour von Bar zu Bar, um die lokalen Kneipen kennenzulernen. Treffpunkt ist vor der Fachschaft, im ETI. (Geb. 11.10)

**Internationales Frühstück**

 - Dieses Frühstück ist für die internationalen Studierenden um sich über die bürokratischen Hindernisse beim Studium am KIT oder in Deutschland auszutauschen. IEH (Geb. 30.36), Room 115.

**Chillen im Park**

 - Gemütliches Ausruhen im schönen Schlosspark von Karlsruhe.

**Bierpong**

 - Ein Bierpongtournier im LTI. (Geb. 30.34)

**Institutsführungen**

 - Eine Führung durch die Institute, die dich in deinem Master begeleiten werden. Treffpunkt ist vor der Fachschaft, im ETI. (Geb. 11.10)

**Hinter den Kulissen**

 - Ein Eiblick hinter die Kulissen. Treffpunkt ist vor der Fachschaft, im ETI. (Geb. 11.10)

**Flunkyball(MIT)**

 - Ein Flunkyballtournier im Audimaxforum. (Geb. 30.95) 

**Museumsnachmittag**

 - Als Studierender hat man Freitags kostenlosen Eintritt in viele Museen in Karlsruhe, deshalb werden wir je nach Andrang einen Ausflug in ein oder mehrere Museen machen. Treffpunkt ist vor der Fachschaft (Geb. 11.10).

<!---
**O-Party**

 - Wir haben für Dich eine große Party im AKK organisiert. Bring richtig Stimmung und ein bisschen Bargeld mit!
--->

<!---
**Scavenger Hunt**

 - Zusammen mit deinem Team löst du Rätsel rund um den Campus und die Stadt, wobei du nochmals den Campus und die Stadt kennen lernst. Im Anshcluss wird es noch etwas Karaoke vor der Fachschaft geben. Treffpunkt vor der Fachschaft (Geb. 11.10).
 --->

{{< calendar ics="https://caldav.fs-etit.kit.edu/public.php/ophase/master" initialDate="2025-04-14" >}}

<!---
**Die Folien der Infoveranstaltung der MO-Phase im April 2024:**
- [MIT](/IntroEvent_MIT.pdf)
- [ETIT](/IntroEvent_StudentCouncil.pdf)
- [Studiengangsservice](/IntroEvent_StudyProgramService.pdf)
--->