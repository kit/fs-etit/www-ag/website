---
title: "Orientierungsphase"
menu:
    main:
        parent: studies
        weight: 5
showDate: true
---


## Was ist die O-Phase?
Die Orientierungsphase, kurz O-Phase, ist ein 1-2 wöchiges Event welches wir für alle angehenden Studierenden an unserer Fakultät Elektro- und Informationstechnik (ETIT) organisieren. In dieser Woche erklären wir alles grundsätzliche zum Studium, führen euch über den Campus und geben euch die Möglichkeit einen ersten Einblik in die vielen Institute unserer Fakultät zu bekommen.
Vor allem ist die O-Phase eine Woche voller Spaß! Hier habt ihr die Chance eure neuen Kommiliton:innen kennenzulernen.
Bei uns trennt sich die O-Phase in die Bachelor-O-Phase und die Master-O-Phase auf. Diese zwei Veranstaltungen überschneiden sich zwar in Teilen, richten sich aber entweder an die Bachelor- oder Masteranfänger. Der folgende Text wird sich größtenteils auf die Bachelor-O-Phase beziehen, ist aber auch auf die Master-O-Phase anwendbar, so weit nicht näher beschrieben.


## Wann?
Die Bachelor O-Phase findet in den 2 Wochen vor dem Vorlesungsstart im Oktober statt. Die Master O-Phase "MoPhase" findet sowohl im Wintersemester als auch im Sommersemester statt. Die Master-O-Phase wird ebenfalls vorraussichtlich in der Woche vor den Vorlesungen stattfinden.


## Wie nehme ich teil?
Wenn du dich für deinen Bachelor oder Master am KIT in Elektrotechnik, Medizintechnik oder Mechatronik eingeschrieben hast, dann erhälst du ein paar Wochen vor der Orientierungsphase eine E-Mail, in der alle wichtigen Informationen und ein Link zur Anmeldung enthalten sind.


## Muss ich an der O-Phase teilnehmen?
An und für sich ist die O-Phase keine Pflichveranstaltung, wir empfehlen allerdings dringen eine Teilnahme. Du wirst in deinem Studium nie wieder eine so gute Möglichkeit haben Komolitonen, die Universität und die Stadt kennenzulernen. Die meisten Studierenden beschreiben die O-Phase als die beste Zeit in ihrem Studium. Lass sie dir auf keinen Fall entgehen!


## Woraus besteht die O-Phase?
Die O-Phase besteht aus einer zweiwöchigen Mischung aus Info-, Kennenlern- und Spaßevents. Eine genaue Auflistung der Events findest du in unserem Bachelor-O-Phasenkalender (bzw. Master-O-Phasenkalender).


## Wer organisiert die O-Phase?
Die Fachschaft ETIT organisiert die O-Phasen. Die Fachschaft ETIT ist die studentische Vertretung aller Studierenden der Fakultät ETIT, also Studierenden der Fächer Elektrotechnik, Medizintechnik und Mechatronik. Wir sind Studierende, die sich neben ihrem Studium für die Studierendenschaft engagieren. Wir organisieren die O-Phase und Feste, vertreten die Meinung der Studierenden gegenüber der Universität und beraten Studierende.
Wenn du mehr über uns erfahren willst, kannst du [hier](https://bestefachschaftderwelt.de/about-us/who-are-we/) und [hier](https://bestefachschaftderwelt.de/about-us/was-machen-wir/) nachlesen.
Wenn du dich während deines Studium sozial engagieren und ein Ehrenamt bestreiten willst, dann bist du bei uns genau richtig. Wenn du zum Mitmachen bei der Fachschaft mehr erfahren willst, dann kannst du [hier](https://bestefachschaftderwelt.de/about-us/participate/) mehr erfahren.

Solltest du Fragen zur O-Phase haben kannst du die O-Phasenorga direkt unter der Mail "o-phase@fs-etit.kit.edu" erreichen.

Da Mechatronik ein gemeinsamer Studiengang der Elektrotechnik und des Maschinenbaus ist, koordiniert der [Arbeitskreis Mechatronik](https://ak-mit.vs.kit.edu/) für die Mechatronikstudierenden eine Kombination der O-Phasen beider Fachschaften, in der Mechatronikstudenten alle Highlights beider O-Phasen mitbekommen.

Veranstalter der O-Phasen im rechtlichen Sinne ist der Fachschaft Elektro- und Informationstechnik Karlsruhe Kasse e.V.


<!--- ## Wie kann ich helfen?
Wenn du bereits in der Fakultät ETIT studierst kannst du dich als Tutor an der Bachelor-O-Phase beteiligen. Den Link zur Anmeldung findest du [hier](https://survey.fs-etit.kit.edu/252681?lang=de-informal). Alle Infos die du dafür benötigst kannst du in den [Infos für Tutoren](/infos_fuer_tutoren.pdf) nachlesen.

Den Link zur Anmeldung zum helfen der Master O-Phase findest du [hier](https://survey.fs-etit.kit.edu/895248?lang=de-informal). --->
<!--- Wir brauchen eine Üergangslösung zwischen den O-Phase für Leute, die helfen wollen --->


## Muss ich irgendwas für die O-Phase verbereiten/bezahlen?
Nein. Dabeisein ist alles. Die Teilnahme an der O-Phase ist komplett kostenlos. Nur bei einigen Events kosten die Getränke und dass Essen etwas. Hab deswegen am besten immer ein bisschen Bargeld dabei. Vorbereiten musst du dich auf die O-Phase nicht.


## Wie komme ich in die O-Phasen-WhatsApp-Gruppe und auf den O-Phasen Discord?
Wir werden die Links für unsere WA-Gruppen und unseren Discord in die Einladungsemail schreiben. Diese Links werden nirgendwo sonst veröffentlicht!!! Alle anderen WA-Gruppen sind inoffiziell. Leider kommt es seit einigen Jahren dazu, dass einige Firmen inoffizielle Studienanfänger-Gruppen erstellen um dann an andere Firmen Werbung in diesen Gruppen zu verkaufen. 


{{< unsafe >}}
<p><br/><a type="button" class="btn btn-success" href='bachelor'>Weitere Informationen zur kommenden Bachelor O-Phase 2025</a><br/></p>
{{</ unsafe >}}

{{< unsafe >}}
<p><br/><a type="button" class="btn btn-success" href='master'>Weitere Informationen zur kommenden Master O-Phase 2025</a><br/></p>
{{</ unsafe >}}

<!---**Alle Informationen zu den kommenden O-Phasen (Bachelor und Master) findet man auf unserer seperaten [O-Phasen Website](https://o-phase.fs-etit.kit.edu)**--->

