---
title: "Orientationphase"
menu:
    main:
        parent: studies
        weight: 5
showDate: true
---


## What is the O-Phase?
The orientation phase, or O-Phase for short, is a 1-2 week event that we organize for all prospective students at our Faculty of Electrical Engineering and Information Technology (ETIT). During this week, we explain everything you need to know about studying, show you around the campus and give you the opportunity to get a first impression of the many institutes in our faculty.
Above all, the O-Phase is a week full of fun! Here you have the chance to get to know your new fellow students.
The O-Phase is divided into the Bachelor O-Phase and the Master O-Phase. Although these two events overlap to some extent, they are aimed at either Bachelor's or Master's first-year students. The following text will mainly refer to the Bachelor's O-Phase, but is also applicable to the Master's O-Phase, unless described in more detail.


## When?
The Bachelor O-Phase takes place in the 2 weeks before the start of lectures in October. The Master O-Phase ‘MoPhase’ takes place in both the winter and summer semesters. The Master O-Phase is also expected to take place in the week before the lectures.

## How do I take part?
If you have enrolled for your Bachelor's or Master's degree in Electrical Engineering, Medical Engineering or Mechatronics at KIT, you will receive an e-mail a few weeks before the orientation phase containing all the important information and a link for registration.


## Do I have to take part in the O-Phase?
In itself, the O-Phase is not a compulsory event, but we strongly recommend that you take part. You will never again have such a good opportunity to get to know fellow students, the university and the city during your studies. Most students describe the O-Phase as the best time of their studies. Don't miss it under any circumstances!


## What does the O phase consist of?
The O-Phase consists of a two-week mix of information, get-to-know-you and fun events. You can find a detailed list of events in our Bachelor O-Phase calendar (Master O-Phase calendar)


## Who organizes the O-Phase?
The ETIT student council organizes the O-Phases. The ETIT student council is the student representation of all students of the ETIT faculty, i.e. students of electrical engineering, medical engineering and mechatronics. We are students who are committed to the student body alongside their studies. We organize the O-Phase and parties, represent the students' opinions to the university and advise students.
If you want to find out more about us, you can read [here](https://bestefachschaftderwelt.de/about-us/who-are-we/) and [here](https://bestefachschaftderwelt.de/about-us/was-machen-wir/).
If you want to get socially involved during your studies and do voluntary work, then you've come to the right place. If you want to find out more about joining the student council, you can find out more [here](https://bestefachschaftderwelt.de/about-us/participate/).

If you have any questions about the O-Phase, you can contact the O-Phase organization directly at “o-phase@fs-etit.kit.edu”.

Since mechatronics is a joint degree program of electrical engineering and mechanical engineering, the [Arbeitskreis Mechatronik](https://ak-mit.vs.kit.edu/) coordinates a combination of the O-phases of both faculties for mechatronics students, in which mechatronics students get all the highlights of both O-phases.

The organizer of the O-phases in the legal sense is the Fachschaft Elektro- und Informationstechnik Karlsruhe Kasse e.V.


## How can I help?
If you are already studying in the ETIT faculty, you can participate as a tutor in the Bachelor-O-Phase. You can find the link to register [here](https://survey.fs-etit.kit.edu/252681?lang=de-informal). You can find all the information you need in the [Info for tutors](/infos_fuer_tutoren.pdf).

You can find the link to register for the Master O-Phase [here](https://survey.fs-etit.kit.edu/895248?lang=de-informal).


## Do I have to prepare/pay anything for the O-Phase?
No. Taking part is everything. Participation in the O-Phase is completely free of charge. Only at some events do the drinks and food cost something. It is therefore best to always have some cash with you. You don't have to prepare for the O-Phase.


## How do I join the O-Phase WhatsApp group and the O-Phase Discord?
We will put the links for our WA groups and our Discord in the invitation email. These links will not be published anywhere else!!! All other WA groups are unofficial. Unfortunately, for some years now, some companies have been creating unofficial freshman groups in order to sell advertising in these groups to other companies.


{{< unsafe >}}
<p><br/><a type="button" class="btn btn-success" href='bachelor'>More information about the upcoming Bachelor O-Phase 2025</a><br/></p>
{{</ unsafe >}}

{{< unsafe >}}
<p><br/><a type="button" class="btn btn-success" href='master'>More information about the upcoming Master O-Phase 2025</a><br/></p>
{{</ unsafe >}}

