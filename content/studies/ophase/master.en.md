---
title: "Master O-Phase"
showDate: true
---

The upcoming Master O phase will take place from 14.04. to 25.04.2025 with various events on all days. You will find a calendar with a detailed list at the end of this text.

In the following, we will briefly explain what awaits you at the individual events:

**The introductory event**

 - The faculty and the student council introduce themselves. We will also explain which events await you during the MO phase. The event will take place in the Engelbert-Arnold-Saal in the ETI (building 11.10) in Engelbert-Arnold-Straße 5, 76131 Karlsruhe.

**Campus Tour**

 - ‘Older’ students will show you around the campus. Meeting point is in front of the student council, in the ETI 
(Building 11.10)

**BBQ**

 - We will meet at the LTI courtyard (building 30.34) and end the evening with a barbecue together. Unfortunately, due to budget restrictions, we cannot offer the food for free, so please bring some cash.

**Prof Breakfast**

 - The name says it all: breakfast with your professors in the LTI Foyer.

**Facts with Simon**

 - We will show you how to navigate through the university IT, which regulations you and the university have to adhere to and give you all the information you need for successful studies. Engelbert-Arnold-Saal, in the ETI. (Building 11.10)

**Pub crawl**

 - A tour from bar to bar to get to know the local pubs. Meeting point is in front of the Fachschaft, in the ETI. (Building 11.10)

 **Café International**

 - This breakfast is for international students to discuss the bureaucratic obstacles to studying at KIT or in Germany. IEH (Building 30.36), Room 115.

**Chill out in the park**

 - Cosy relaxation in the beautiful Karlsruhe Palace Park.

**Beer pong**

 - A beer pong tournament at the LTI. (Geb. 30.34)

**Guided tours of the institutes**

 - A guided tour through the institutes that will accompany you in your Master's programme. Meeting point is in front of the student council, in the ETI. (Building 11.10)

**Behind the scenes**

 - A look behind the scenes. Meeting point is in front of the student council, in the ETI. (Building 11.10)

**Flunkyball(MIT)**

 - A flunkyball tournament in the Audimaxforum. (Building 30.95) 

**Museum afternoon**

 - As a student, you have free entry to many museums in Karlsruhe on fridays, so we will be taking a trip to one or more museums depending on how busy they are. Meeting point is in front of the student council (Building 11.10).

<!---
**Scavenger Hunt**

 - Together with your team, you will solve puzzles around the campus and the city, getting to know the campus and the city once again. Afterwards there will be some karaoke in front of the student council. Meeting point in front of the student council (building 11.10).

**O-Party**

 - We have organized a big party for you at the AKK. Bring a good mood and some cash!
--->

{{< calendar ics="https://caldav.fs-etit.kit.edu/public.php/ophase/master" initialDate="2025-04-14" >}}

<!---
**Below, you'll find the slides of our introductory events from April 2024:**
- [MIT](/IntroEvent_MIT.pdf)
- [ETIT](/IntroEvent_StudentCouncil.pdf)
- [Study Program Service](/IntroEvent_StudyProgramService.pdf)
--->