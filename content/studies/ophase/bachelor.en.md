---
title: "Bachelor O-Phase"
showDate: true
---

Planning for the O-phase for the upcoming winter semester 2025/26 is underway.

<!---
The upcoming Bachelor O-Phase will take place from October 7th, 2024 to October 18th, 2024, with various events scheduled for each day. You can find a detailed calendar listing all the events at the end of this text. To get a rough estimate of the number of participants, we kindly ask you to fill out the registration form for the O-Phase, which was sent as a link in the email from the faculty. If you haven't received an email yet, please be patient; you should receive it soon.

We would also like to remind you that we have sent you the links to our communication channels and the T-shirt shop via email. Please note: The links for the WhatsApp groups are only sent to enrolled first-semester students via email. Any other link is unofficial and, in case of doubt, a commercial advertising channel from external companies.

Our O-Phase calendar can be found at the very end of this page, and you can also download it [here](/ophase_ba.ics) as an iCal (.ics) file. Meeting points are included in the calendar.

In general, attendance is not mandatory during the O-Phase, but participation is highly recommended. It's best to arrive 10 minutes before the events start. Plan ahead where and when you will eat during breaks, and always carry some cash with you.

The highlights of the O-Phase are:

**The Introduction Event**

- The faculty will introduce itself, there will be a small introductory test, and you will be introduced to the O-Phase.

**The Campus Tour**

- Older students ("Kommilitonen"), called tutors during the O-Phase, will show you around the campus.

**The Welcome BBQ**

- We will meet at the LTI courtyard and end the evening with a barbecue. Unfortunately, due to budget constraints, we cannot offer the food for free, so please bring some cash.

**The IBU Lecture**

- Unfortunately, due to scheduling conflicts, a lecture couldn't be held during the semester and will be moved to the O-Phase. It is highly recommended to bring writing materials.

**The O-Rally**

- The absolute highlight of the O-Phase. We have 14 stations spread across the city where you will need to complete various tasks. It's a perfect way to make new acquaintances and get to know the city.

**The O-Phest**

- We've organized a big party for you at the AKK. Bring lots of energy and some cash!

**Poker Night**

- As a university of excellence, we have to show some class! Accordingly, we will have a poker night in style (of course, not for real money). We would like to remind you of our dress code; please come in appropriate attire, as we don't want to look like the student riffraff of RWTH or TUM.

**Facts with Simon**

- We will show you how to navigate the university IT, which regulations you and the university must adhere to, and give you all the information you need for a successful study.

{{< calendar ics="https://caldav.fs-etit.kit.edu/public.php/ophase/bachelor" initialDate="2024-10-07" >}}
--->
