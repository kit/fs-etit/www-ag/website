---
title: "Bachelor O-Phase"
showDate: true
---

Die Planungen der O-Phase für das kommende Wintersemester 2025/26 sind im Gange.

<!--- 
Die kommende Bachelor O-Phase wird vom 07.10.2024 bis zum 18.10.2024 mit verschiedenen Events an allen Tagen stattfinden. Einen Kalender mit der Detaillierten Auflistung findest Du im Anschluss dieses Textes. Für eine grobe Abschätzung an Teilnehmenden bitten wir Dich, die Anmeldung zur O-Phase, welche als Link in der Mail von der Fakultät kam, auszfüllen. Solltest du keine Mail erhalten haben, gedulde dich noch etwas, dann wirst du sie bald erhalten.

An dieser Stelle möchten wir Dich auch nochmal aufmerksam machen, dass wir Dir die Links zu unseren Kommunikationskanälen und dem T-Shirtshop per E-Mail gesendet haben. Hinweis: Die Links für die WhatsApp-Gruppen werden nur per Mail an Immatrikulierte Erstsemester gesendet. Jeder andere Link ist nicht offiziell und im Zweifelsfall ein kommerzieller Werbekanal externer Firmen.

Unser O-Phasenkalendar steht ganz am Ende dieser Seite, Du kannst ihn dir auch [hier](/ophase_ba.ics) als ICal (.ics-Datei) herunterladen. Die Treffpunkte sind im Kalendar hinterlegt.

Grundsätzlich gilt in der O-Phase keine Anwesenheitspflicht, eine Teilnahme an der O-Phase ist aber dringend zu empfehlen. Sei am besten 10 Minuten vor Beginn der Events da. Überlege Dir im Vorhinein wann und wo Du in den Pausen Essen gehen kannst und hab immer ein bisschen Bargeld an Dir.

Die Highlights der O-Phase sind:

**Die Einführungsveranstaltung**

 - Die Fakultät stellt sich vor, es findet ein kleiner Einführungstest statt und Dir wird die O-Phase vorgestellt.

**Die Campustour**

- Ältere Studierende ("Kommilitonen"), in der O-Phase Tutoren genannt, zeigen Dir den Campus.

**Das Kennenlerngrillen**

- Wir treffen uns auf dem LTI-Hof und lassen den Abend bei gemeinsamen Grillen ausklingen. Leider können wir aufgrund von Budget-Beschränkungen das Essen nicht kostenlos anbieten, bring also bitte ein bisschen Bargeld mit.

**Die IBU-Vorlesung**

- Leider konnte eine Vorlesung aufgrund von terminlichen Problemen nicht unter dem Semester gehalten werden und wird deswegen in die O-Phase vorverlegt. Es wird dringend empfohlen Schreibmaterial mitzubringen.

**Die O-Rallye**

- Das absolute Highlight der O-Phase. Wir haben 14 Stationen durch die Stadt verteilt, an denen Du verschiedenste Aufgaben lösen must. Perfekt um Bekanntschaften zu machen und die Stadt kennenzulernen.

**Das O-Phest**

- Wir haben für Dich eine große Party im AKK organisiert. Bring richtig Stimmung und ein bisschen Bargeld mit!

**Der Pokerabend**

- Als Exzellenzuniversität müssen wir natürlich Klasse zeigen! Dementsprechend werden wir in gehobener Klasse gemeinsam Pokern (natürlich nicht um echtes Geld). Wir möchten an dieser Stelle auf unseren Dresscode hinweisen, wir bitten in angemessener Kleidung zu kommen, wir wollen ja nicht wie der Hochschulpöbel der RWTH oder TUM rüberkommen.

**Fakten mit Simon**

- Wir zeigen Dir wie Du durch die Uni-IT navigierst, an welche Regularien Du und die Uni sich halten müssen und geben Dir alle Informationen, die du für ein erfolgreiches Studium benötigst.




{{< calendar ics="https://caldav.fs-etit.kit.edu/public.php/ophase/bachelor" initialDate="2024-10-07" >}} 
--->
