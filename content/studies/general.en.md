---
title: General
menu:
    main:
        parent: studies
        weight: 1
showDate: true
---

On this page you will find a lot of important and relevant information about studying in general and specifically about studying at our faculty. If you have any questions, you can also always contact us via [e-mail](mailto:info@fs-etit.kit.edu).

## Study
- [Registration and deregistration for exams](https://campus.studium.kit.edu/exams/index.php)
- {{< relurl "studies/timetables" >}}Timetables{{</ relurl >}}
- [Registration for the new semester](https://campus.studium.kit.edu/renewal/index.php)
- [Study certificates (matriculation notice, KVV, ...)](https://campus.studium.kit.edu/reports/studyreports.php)
- [Foreign countries and other trips](https://www.intl.kit.edu/ostudent/index.php)
- [Internship Office](https://www.etit.kit.edu/praktikantenamt.php)
- [BAföG](https://www.sw-ka.de/de/finanzen/bafoeg/)
- [Wifi](https://www.scc.kit.edu/dienste/wlan.php)
- [Printing scripts or other documents](https://www.asta-kit.de/de/angebote/druckerei)
- [Free study places](https://www.bibliothek.kit.edu/cms/freie-lernplaetze-karte.php)

## Exam dates
- [ETIT Bachelor Exams](https://www.etit.kit.edu/pruefungstermine_bachelor.php)
- [ETIT Master's Exams](https://www.etit.kit.edu/pruefungstermine_master.php)


## MINT College
- [Before study](https://www.mint-kolleg.kit.edu/Studienvorbereitung.php)
- [During studies](https://www.mint-kolleg.kit.edu/Studienbegleitung.php)

## Additional qualifications
- [Language courses](https://spz.kit.edu/253.php)
- [HoC](https://studium.hoc.kit.edu/index.php/anmeldeverfahren)
- [ZaK](https://zak.kit.edu/anmeldung.php)

## Recreational activities
- [Sports courses](https://www.ifss.kit.edu/hochschulsport/sportangebot.php)
- [University groups](https://www.asta-kit.de/de/engagier-dich/hochschulgruppen/gefoerdert)

## Problems
- [Failed exam, achievements are not credited](https://www.etit.kit.edu/beratung.php)
- [Study is no fun anymore, you are thinking about dropping out or you need help to continue](https://www.sle.kit.edu/imstudium/zib.php)
- [Uni is dumb, flat share is dumb, life is dumb, everything is dumb](https://www.sw-ka.de/de/beratung/psychologisch/psychotherapeutische_beratungsstelle_karlsruhe/)
- ["I'll sue you!" - and other legal problems](https://www.asta-kit.de/de/angebote/beratung/rechtsberatung)
