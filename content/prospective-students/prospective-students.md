---
title: Studieninteressierte
menu:
    main:
        parent: prospective-students
        weight: 1
---

- Du interessierst dich für das Studium der Elektro- und Informationstechnik, der Medizintechnik oder der Mechatronik am KIT?
- Du hast Lust dich in die weite Welt von Nanoelektronik bis Automatisierungstechnik stürzen?
- Du bist motiviert und möchtest ein spannendes und zukunftsgerichtetes Ingenieursstudium anstreben?

Dann bist du hier genau richtig!

Am KIT hast du die Möglichkeit, ein aufeinander abgestimmtes Bachelor- und Masterstudium zu beginnen und dabei die Grundlagen der Elektro- und Informationstechnik, sowie der Medizintechnik kennenzulernen und dich in einer der vielen Vertiefung zu spezialisieren. Dabei bildet das Studium immer wieder die Brücke zur Anwendung und bietet einige Möglichkeiten gelerntes Wissen praktisch anzuwenden. Den genauen Ablauf des Studiums findest du hier.

Natürlich hast du jetzt viele Fragen, „Was heißt Studieren?“; „Ist ein Studium an der Uni das Richtige für mich?“; „Wie, wann und wo kann ich mich bewerben?“ etc.. Einige Fragen können wir dir sicherlich am Studieninformationstag, in unserem FAQ, per Mail oder natürlich auch persönlich in der Fachschaft beantworten. Außerdem gibt es das „ZIB“, das „Zentrum für Information und Beratung“, um eure Fragen zu klären und euch persönliche Beratung anzubieten. Dort findet ihr Beratung für Studienanfänger und erhaltet weitere interessante Informationen rund um das Studium der Elektro- und Informationstechnik, Mechatronik und der Medizintechnik. 
 
Wir freuen uns über dein Interesse und hoffen dich bald persönlich in der Fachschaft, in unserer O-Phase oder auch einfach so auf dem Campus kennenzulernen.

**Deine Fachschaft Elektro- und Informationstechnik**

