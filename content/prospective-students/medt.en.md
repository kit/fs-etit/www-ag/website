---
title: "Medical Technologies"
menu:
    main:
        parent: prospective-students
        weight: 4
---

## Bachelor
The Bachelor's degree in Medical Engineering focuses on the electrical engineering and information technology aspects of medical engineering, in particular imaging techniques (ultrasound, MRI, etc.) and the processing of electrical signals from the body. The Bachelor's degree therefore includes many of the basic subjects of the Electrical Engineering Bachelor's degree. The medical technology part of the degree consists of the modules "Physiology & Anatomy I for Medical Technology", "Physiology & Anatomy II for Medical Technology", "Introduction to Medical Technology" and "Special Topics in Medical Technology". In addition, there is the practical course "Applied Medical Technology" in the 3rd semester. This is held in cooperation with the Faculty of Medicine in Mannheim and includes an excursion to the surgical department there.
As the Bachelor's degree is very close to the Electrical Engineering & Information Technology course, it is possible to switch to the Electrical Engineering Master's degree after successfully completing the Medical Engineering Bachelor's degree. 

Important: If you would like to have modules from a previous degree course or an apprenticeship credited, you must do this in your first Bachelor's semester. To do this, please contact the [Bachelor's degree program service](https://www.etit.kit.edu/studiengangservice_bachelor_etit_medt_mit.php)

You can find more information about the Medical Engineering Bachelor's degree on the website of the [faculty](https://www.etit.kit.edu/bachelor_medizintechnik.php)

