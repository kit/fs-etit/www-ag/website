---
title: Elektrotechnik & Informationstechnik 
menu:
    main:
        parent: prospective-students
        weight: 2
---
## Bachelor
Die ersten vier Semester vermitteln die Grundlagen in allen Bereichen der Elektro- und Informationstechnik sowie in Mathematik und Physik. Im 5. und 6. Semester - wenn ihr ggf. schon wisst welcher Bereich dich am meisten interessiert - kannst du in einer der Vertiefungsrichtungen, dem Berufspraktikum und mit der Bachelorarbeit Schwerpunkte nach eigenen Interessen setzen.
Die Wahl der Vertiefungsrichtung im Bachelor hat keinen Einfluss auf die Wahl der Vertiefungsrichtung im Master. Es ist also absolut möglich, sich thematisch noch einmal umzuentscheiden wenn man in den Master wächselt.

Derzeitig werden folgende Vertiefungsrichtungen angeboten:
- Allgemeine Elektrotechnik & Informationstechnik (AEI)
- Automatisierung, Robotik & Systems Engineering (ARS)
- Elektrische Energiesysteme und Elektromobilität (EEE)
- Informations- & Kommunikationstechnik (IKT)
- Mikroelektronik, Photonik und Quantentechnologien (MPQ)

Wichtig: Wenn du dir Module aus einem vorherigen Studium oder einer Ausbildung anrechnen lassen möchtest, musst du dies in deinem 1. Bachelor Semester tun. Hierfür trete bitte in Kontakt mit dem [Bachelor Studiengangsservice](https://www.etit.kit.edu/studiengangservice_bachelor_etit_medt_mit.php)

Weitere Informationen zum ETIT Bachelor findest du auch auf der Website der [Fakultät](https://www.etit.kit.edu/bachelor_etit.php) Wichtige Regelungen sind der Bachelorprüfungsordnung und dem Modulhandbuch zu entnehmen.

## Master
Der Master ermöglicht dir, dich in einem Feld der Elektrotechnik & Informationstechnik zu vertiefen. Derzeit gibt es 25 Vertiefungsrichtungen aus denen man wählen kann. 
Jede Vertiefungsrichtung setzt sich aus einem Grundlagen-, einem Pflicht- und einem Wahlteil zusammen. Zu Beginn des Masterstudiums, empfiehlt es sich einmal mit der Fachstudienberater:in der Vertiefungsrichtung in Kontakt zu treten um sich ggf. zu der Vertiefung beraten zu lassen und mögliche Wahlmodule zu besprechen. 
Da viele Vertiefungen sich themaitsch überschneiden, ist es teilweise auch im Master noch möglich zwischen Vertiefungsrichtungen zu wechseln. Solltest du am Anfang des Master Studiums noch unsicher sein, kannst du  erst mal Kurse wählen, die sich in mehreren Vertiefungen die dich interessieren, wieder finden. Bevor du die Masterarbeit startest, sollte jedoch klar sein welche Vertiefung du machst. Zum Start der Master Arbeit müssen auch mindestens 75 ECTS deiner Speizialisierung bereits abgelegt wurden sein.

Wichtig: Wenn du dir etwas aus einem vorherigen Studium oder vom Mastervorzug anrechnen lassen möchtest, musst du dies in deinem 1. Master Semester tun. 

Weitere Informationen zum ETIT Master findest du auch auf der [Website der Fakultät](https://www.etit.kit.edu/master_etit.php). Wichtige Regelungen sind der Masterprüfungsordnung und dem Modulhandbuch zu entnehmen.