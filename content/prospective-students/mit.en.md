---
title: "Mechatronics & Information Technologies"
menu:
    main:
        parent: prospective-students
        weight: 3
---

The Mechatronics degree program is offered jointly by the Faculty of Electrical Engineering & Information Technology and the Faculty of Mechanical Engineering. Accordingly, both faculties and both student councils are the contact partners. In addition, there is the Mechatronics Working Group (AK-MIT), which is an association of mechatronics students and is committed to the interests of mechatronics students. Contact persons and further information about the working group can be found on its website[https://www.ak-mit.vs.kit.edu/]

## Bachelor
The Mechatronics and Information Technology program combines knowledge from mechanical engineering, such as technical mechanics and machine design, with that of electrical engineering and information technology, such as digital technology or control engineering. In the first four semesters, you will learn the basic content of these disciplines. From the 4th semester onwards, you also have the opportunity to deepen your knowledge in certain disciplines with the elective area of specialization. In the 5th semester, theory is put into practice as part of the "Mechatronic Systems and Products Workshop" course. You can continue to set your own priorities with the work placement and Bachelor's thesis in the 6th semester. 

Important: If you would like to have modules from a previous course of study or training recognized, you must do this in your first Bachelor's semester. To do this, please contact the [Bachelor's degree program service](https://www.etit.kit.edu/studiengangservice_bachelor_etit_medt_mit.php)

You can also find more information about the MIT Bachelor on the [website of the central study program service](https://www.sle.kit.edu/vorstudium/bachelor-mechatronik-informationstechnik.php)Important regulations can be found in the Bechlor's examination regulations and the module handbook.

## Master 
The Master's in Mechatronics & Information Technology consists of a compulsory part "General Mechatronics", a specialization and an interdisciplinary elective area.
The following specializations are currently available.

-Vehicle Technology
-Energy technology
-Microsystems technology
-medical technology
-industrial automation
-Control engineering in mechatronics
-Robotics
-Construction of mechatronic systems

In the elective area, you can choose freely from the modules in the module handbook of the degree program. Care should be taken to ensure that the elective modules match the specialization. If you would also like to choose modules from the module handbook of the Electrical Engineering or Mechanical Engineering Master's program, you must apply for this at [Studiengangsservice](https://www.etit.kit.edu/studiengangservice_master_etit_und_mit.php). 

Important: If you would like to have something from a previous degree course or from the Master's preference recognized, you must do this in the first semester of the Mastr's program. 

You can also find further information on the MIT Master's program on the [central study program service](https://www.sle.kit.edu/vorstudium/master-mechatronik-informationstechnik.php) Important regulations can be found in the Master's examination regulations and the module handbook.