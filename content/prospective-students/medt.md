---
title: "Medizintechnik"
menu:
    main:
        parent: prospective-students
        weight: 4
---

## Bachelor
Der Bachelor Medizintechnik, fokussiert sich auf die elektrotechnischen und informationstechnischen Aspekte der Medizintechnik.Diese sind vor allem die bilgebenden Verfahren (Ultraschall, MRT etc.) und die Verarbeitung elektrischer Signale des Körpers. Deshalb enthält der Bachelor viele der Grundlagenfächer des Elektrotechnik Bachelors. Der medizintechnische Teil des Bachelors setzt sich aus den Modulen "Physiologie & Anatomie I für die Medizintechnik", "Physiologie & Anatomie II für die Medizintechnik", "Einführung in die Medizintechnik" und "Spezielle Themen der Medizintechnik" zusammen. Zusätzlich gibt es im 3. Semester das Praktikum "Angewandte Medizintechnik". Dieses wird in Kooperation mit der Fakultät für Medizin in Mannheim gehalten und ermöglicht eine Exkursion in die dortige Chirurgie.
Dadurch, dass der Bachelor recht nah am Studiengang Elektrotechnik & Informationstechnik dran ist, ist es möglich nach erfolgreichem Abschluss des Medizintechnik Bachelors in den Elektrotechnik Master zu wechseln. 

Wichtig: Wenn du dir Module aus einem vorherigen Studium oder einer Ausbildung anrechnen lassen möchtest, musst du dies in deinem 1. Bachelor Semester tun. Hierfür trete bitte in Kontakt mit dem [Bachelor Studiengangsservice](https://www.etit.kit.edu/studiengangservice_bachelor_etit_medt_mit.php)

Weitere Informationen zum Medizintechnik Bachelor findest du auf der Website der [Fakultät](https://www.etit.kit.edu/bachelor_medizintechnik.php). Wichtige Regelungen sind der Bachelorprüfungsordnung und dem Modulhandbuch zu entnehmen.

## Master
Ein Master der Medizintechnik ist zur Zeit noch in Planung und wird voraussichtlich im WS25/26 starten.

