---
title: Electrical Engineering & Information Technologies 
menu:
    main:
        parent: prospective-students
        weight: 2
---
## Bachelor
The first four semesters teach the basics in all areas of electrical engineering and information technology as well as mathematics and physics. In the 5th and 6th semester you can choose one of the bachelor specializations and use your internship and/or bachelor thesis to set a focus on an area that interests you the most.
The choice of specialization in the Bachelor's degree has no influence on the choice of specialization in the Master's degree. It is therefore perfectly fine to change your choice of specialization when you move on to the Master's degree.

The following specializations are currently offered in the Bachelor degree program:
- General Electrical Engineering & Information Technology (AEI)
- Automation, Robotics & Systems Engineering (ARS)
- Electrical Energy Systems and Electromobility (EEE)
- Information & Communication Technology (ICT)
- Microelectronics, Photonics and Quantum Technologies (MPQ)

Important: If you would like to have something from your previous studies or an apprenticeship credited to you Bachelor degree, you must do this in your 1st Bachelor semester by reaching out to the faculty's [bachelor degree program service](https://www.etit.kit.edu/studiengangservice_bachelor_etit_medt_mit.php).

You can find more information about the ETIT Bachelor on the website of the [faculty](https://www.etit.kit.edu/bachelor_etit.php)

## Master
The Master's degree enables you to specialize in a field of electrical engineering & information technology. There are currently 25 specializations to choose from. 
Each specialization consists of a basic, a compulsory and an elective part. At the beginning of the Master's program, it is advisable to contact the academic advisor of your chosen specialization to get some advice about the specialization and to discuss possible elective modules. 
As many specializations overlap thematically, it is still possible to switch between specializations during the Master's degree. Hence, it is not mandatory to choose your specialisation richt at the beginning. If you are still unsure at the beginning of the Master's program, you can first choose courses that can be found in several specializations that interest you. Before starting you master thesis though, you must have chosen your final specialization, have finished at least 75 credit points of that specialisation and got your cource choices signed by the specialization advisor.

Important: If you would like to transfer credits from a previous degree course or from the "Master Vorzug" of your bachelor degree, you must do this in your first semester by getting in contact with the [master degree program service](https://www.etit.kit.edu/studiengangservice_master_etit_und_mit.php)

You can find more information about the ETIT Master's program on the website of the [faculty](https://www.etit.kit.edu/english/master_etit.php) Important regulations concerning the program can also be found in the examination regulations and the module handbook.