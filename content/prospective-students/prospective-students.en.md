---
title: Prospective students
menu:
    main:
        parent: prospective-students
        weight: 1
---

- Are you interested in studying electrical and information engineering or medical technology at KIT?
- You want to plunge into the wide world of nanoelectronics to automation technology?
- You are motivated and want to pursue an exciting and future-oriented engineering study?

Then this is the right place for you!

At KIT, you have the opportunity to start a coordinated bachelor's and master's degree program and to get to know the basics of electrical engineering and information technology as well as medical engineering and to specialize in one of the many specializations. At the same time, the study program is always the bridge to application and offers several opportunities to apply learned knowledge in practice. You can find the exact course of studies here.

Of course you have many questions now, "What does studying mean?"; "Is studying at a university the right thing for me?"; "How, when and where can I apply?" etc.. We can certainly answer some of your questions at the study information day, in our FAQ, by mail or of course personally at the student council. There is also the "ZIB", the "Center for Information and Counseling", to answer your questions and offer you personal counseling. There you can find advice for first-year students and get further interesting information about the study of Electrical and Information Engineering and Medical Engineering. 
 
We are happy about your interest and hope to meet you soon in person at the student council, in our O-Phase or simply on campus.

**Your student council for electrical engineering and information technology**.

