---
title: "Mechatronik & Informationstechnik"
menu:
    main:
        parent: prospective-students
        weight: 3
---
Der Mechatronik Studiengang wird geminsam von der Fakultät für Elektrotechnik & Informationstechnik und der Fakultät für Maschinenbau angeboten. Es gelten demenstprechend beide Fakultäten und beide Fachschaften als Ansprechpartner. Zusätzlich gibt es den Arbeitskreis Mechatronik (AK-MIT) der ein Zusammenschluss aus Mechatronik Studierenden ist und sich für die Belänge der Mechatronik Studierenden einsetzt. Ansprechpartner:innen und weitere Informationen zu dem Arbeitskreis findet man auf seiner [Website](https://www.ak-mit.vs.kit.edu/)

## Bachelor
Die Studiengang Mechatronik und Informationstechnik verbindet Wissensinhalte des Maschinenbaus, wie Technische Mechanik und Maschinenkonstruktionslehre, mit denen der Elektrotechnik und Informationstechnik, z.B. Digitaltechnik oder Regelungstechnik. In den ersten vier Semestern erlernst du die grundlegenden Inhalte dieser Disziplinen. Ab dem 4. Semester hat man außerdem die Möglichkeit sich mit dem Wahlbereich eigenstäding in bestimmten Disziplinen zu vertiefen. Im 5. Semester wird im Rahmen der Veranstaltung "Workshop Mechatronische Systeme und Produkte" die Theorie in die Praxis gebracht. Mit dem Berufspraktikum und der Bachelorarbeit im 6. Semester kann man weiterhin eigene Schwerpunkte setzen. 


Wichtig: Wenn du dir Module aus einem vorherigen Studium oder einer Ausbildung anrechnen lassen möchtest, musst du dies in deinem 1. Bachelor Semester tun. Hierfür trete bitte in Kontakt mit dem [Bachelor Studiengangsservice](https://www.etit.kit.edu/studiengangservice_bachelor_etit_medt_mit.php)

Weitere Informationen zum MIT Bachelor findest du auch auf der [Website des zentralen Studiengangsservice](https://www.sle.kit.edu/vorstudium/bachelor-mechatronik-informationstechnik.php)
Wichtige Regelungen sind der Bachelorprüfungsordnung und dem Modulhandbuch zu entnehmen.

## Master 

Der Master Mechatronik & Informationstechnik besteht aus einem Pflichtteil "Allgemeine Mechatronik", einer Vertiefung und einem interdisziplinären Wahlbereich.
Folgende Vertiefungen sind zur Zeit zur Auswahl.

-Fahrzeugtechnik
-Energietechnik
-Mikrosystemtechnik
-Medizintechnik
-Industrieautomation
-Regelungstechnik in der Mechatronik
-Robotik
-Konstruktion Mechatronischer Systeme

Im Wahlbereich kann man frei aus den Modulen im Modulhandbuch des Studiengangs wählen. Hierbei sollte darauf geachtet werden, dass die Wahlmodule zur Vertiefung passen. Falls man außerdem Module aus dem Modulhandbuch des Elektrotechnik oder des Maschinenbau Masters wählen möchte, muss man dies beim [Studiengangsservice](https://www.etit.kit.edu/studiengangservice_master_etit_und_mit.php) beantragen. 

Wichtig: Wenn du dir etwas aus einem vorherigen Studium oder vom Mastervorzug anrechnen lassen möchtest, musst du dies in deinem 1. Master Semester tun. 

Weitere Informationen zum ETIT Master findest du auch auf der Website des [zentralen Studiengangsservice](https://www.sle.kit.edu/vorstudium/master-mechatronik-informationstechnik.php) Wichtige Regelungen sind der Masterprüfungsordnung und dem Modulhandbuch zu entnehmen.