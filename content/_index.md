---
title: Startseite
header: home_header.webp
---

Herzlich Wilkommen auf der Internetpräsenz der **Fachschaft Elektro- und Informationstechnik am Karlsruher Institut für Technologie**. Wir sind die Vertretung aller Studierenden der Fakultät für Elektrotechnik und Informationstechnik (ETIT) des KIT.

Die Fachschaft Elektro- und Informationstechnik ist Teil der Verfassten Studierendenschaft am KIT. Wir bieten zahlreiche Services rund ums Studium (der Elektro- und Informationstechnik, Mechatronik und Informationstechnik und Medizintechnik) an.

In Problemfällen mit Prüfungen oder eurem Praktikum beraten wir euch gerne während unserer Öffnungszeiten, ihr könnt euch aber auch direkt an die studentischen Vertreter in BPA und MPA wenden.

Insbesondere für Neueinsteiger oder Studieninteressierte bieten wir Beratung rund um das Studium und Karlsruhe an.

Wenn ihr Fragen, Anregungen oder Kritik habt, tretet doch einfach mit uns in Kontakt oder nehmt mittwochs um 18 Uhr an einer Fachschaftssitzung teil.

Um unsere Aufgaben erfüllen und unsere Dienstleistungen anbieten zu können, suchen wir immer engagierte Menschen. Wenn du dich dazu zählst und Lust an Mitarbeit hast, kommt einfach in der Fachschaft vorbei!
