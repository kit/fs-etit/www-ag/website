---
title: FAQ
menu: main
weight: 6
showDate: true
---

{{< accordion "faq" >}}

{{% accordion-item "I have to apply for an extension/exam repetition, what do I do now?" %}}
We have put together a few tips for you: 
- Read the study regulations carefully. Here you will find all the important information on maximum duration of study, examination regulations and so on.
- Talk to the BPA/MPA contact person in the student council. They can answer your questions and give you tips on how to submit a formal application to the examination board correctly.
- You can also contact the examination board directly to find out about the application process. The application must also be submitted to the secretariat at the end.
- Stay calm. It's not the end of the world. You are not the first and will not be the last to be in this situation. 
{{%/ accordion-item %}}

{{% accordion-item "I am having problems with the online exam registration. Who can help me?" %}}
Contact the faculty's study program service. They can register you for the desired exam in the campus system. 
- Email: [Bachelor Service](mailto:bachelor-info@etit.kit.edu), [Master Service](mailto:master-info@etit.kit.edu) 
- Websites: [Bachelor Study Program Service](https://www.etit.kit.edu/studiengangservice_bachelor_etit_medt_mit.php), [Master Study Program Service](https://www.etit.kit.edu/studiengangservice_master_etit_und_mit.php) 
{{%/ accordion-item %}}

{{% accordion-item "Is there also an orientation week for external Master's students?" %}}
Yes, there is also an orientation week for new Master's students. The so-called Mo-phase is usually smaller than the Bachelor orientation week. You can find the program and other important information on the [website of the orientation week](https://o-phase.fs-etit.kit.edu).
{{%/ accordion-item %}}

{{% accordion-item "What are key qualifications and which semester is best for me to do them?" %}}
During your Bachelor's and Master's degree, you have to gain a few ECTS through so-called key qualifications. How many ECTS you need for your degree is set out in the examination regulations. Key qualifications include, for example, soft skills, languages or learning techniques.

They are not prescribed for a specific semester, but it is advisable to take care of them early on - not directly in the first semester, but also not during the Bachelor's thesis.

There are many interesting offers at the HoC, ZAK and Sprachzentrum (SPZ) from which you can learn a lot. But you can also find some key qualifications in the module handbook, which are offered directly by the faculty.
{{%/ accordion-item %}}

{{% accordion-item "How do the tutorials work? Can there be collisions?" %}}
In ETIT and MEDT, times are usually allocated via the WiWi portal or the new Campus Plus portal.

There you can rate all tutorial times from 1 to 5 stars so that you don't get any unfavorable (1 star) times. You can find more information there. There can be no collisions within the same portal.

At MIT there is also the Redseat portal. Unfortunately, there have been collisions between Redseat and other portals in recent years. Should this happen to you, the times of the TM tutorials and MKL workshop are unfortunately not negotiable. For the other tutorials, you can find a new one by simply coming to the time and place of the tutorial and asking if there are still places available. 
{{%/ accordion-item %}}

{{% accordion-item "How can I find out more about studying at KIT?" %}}
Every year in November, the Study Information Day takes place, where you can get lots of interesting information and even attend lectures. Pupils from Baden-Württemberg are also given time off from school for this. In addition, the Campus Day takes place every year on a Saturday in May. Here, too, there are many events and information stands spread across the campus where you can find out more about KIT and studying at KIT.
You are also welcome to take a walk around the campus and visit the student council. In summer, we often organize campus tours for prospective students.

If you are interested in taking part in one, please get in touch by email. We look forward to getting to know you.

If you are looking for advice on the various degree programs at KIT, you can also go to the [Central Student Advisory Service](https://www.sle.kit.edu/wirueberuns/zsb.php). The offices are in the KIT Presidium directly opposite our student council. 
{{%/ accordion-item %}}


{{% accordion-item "Where can I get lecture notes?" %}}
You can find most of them on the lecture homepages or in ILIAS and print them out yourself in the SCC, for example.

For MIT students, the script sales in the canteen are more interesting, as the HMI-III and TMI-II scripts are sold there. 
{{%/ accordion-item %}}



{{% accordion-item "Which laptop/calculator should I buy?" %}}
**ETIT & MEDT:** First of all, there is no requirement, most people have a Lenovo ThinkPad (also because of the discounts for students and the availability of used devices). An inexpensive laptop quickly reaches its limits when it comes to programming and computing-intensive simulations. However, these limits are usually not reached in the Bachelor lectures, and the [scientific computing center](https://www.scc.kit.edu/dienste/pools.php) offers workstations that can be used during opening hours. However, a good battery is necessary, as many study areas and lecture halls unfortunately do not have many sockets.
It is best to take a look at the system requirements for the MatLab program.

**MIT:** You need to consider whether you want to specialize in MKL3/4. If this is the case, then take a look at the system requirements for the CAD program Creo. There are also many students who take lecture notes with an iPad or Surface. The purchase of such devices is also not a must. It is up to you whether you find them useful. 

Otherwise, the same information applies as for the ETITs and MEDTs



**ETIT/MEDT/MIT:** On the subject of calculators: Don't buy a graphics-capable and programmable calculator, as they are not allowed in the exams. Most people have the Casio fx-991: a simple pocket calculator that has many constants stored in it and can do everything that is allowed in exams.
Many students now use a tablet or laptop with a touchscreen to take notes during lectures. Since the slide sets of lectures are usually uploaded in advance, you can take notes directly on the slides.
{{%/ accordion-item %}}



{{% accordion-item "What software can I get for free as a student??" %}}
First of all, quite a lot. You can find most of it in the [SCC Softwareshop](https://www.scc.kit.edu/dienste/softwareshop.php), including Matlab, LabView and all Microsoft operating systems.

Also, of course, open source software ;-)
{{%/ accordion-item %}}



{{% accordion-item "Do you also offer excursions?" %}}
Various institutes offer excursions. They are usually announced in the lecture and on their home pages. 

If we offer excursions, they will also be announced in the lecture via this website and our social media channels. You must register on site during one of our opening hours.
{{%/ accordion-item %}}


{{% accordion-item "What do I have to do to change my field of study?" %}}
You must apply for your new desired degree program in the same way as the first time. In addition, you must provide written proof of academic counseling related to the desired degree program.

If you would like to have subjects from your first degree program credited, you should clarify this directly with the relevant lecturers. The crediting must be done within the 1st semester!
{{%/ accordion-item %}}



{{% accordion-item "Where can I find the timetable and when will it be updated?" %}}
You can find the timetable for the Bachelor's degree program ETIT & MEDT [here](/en/studies/timetables).

You can find the timetable for the Bachelor's degree program MIT [here](https://www.mach.kit.edu/vorlesungsverzeichnis.php).

Please note that only your compulsory subjects are listed here. Specialization subjects can be found on the respective institute pages.
{{%/ accordion-item %}}



{{% accordion-item "When are you open? When can I purchase exams/examination transcripts?" %}}
During the lecture period, we normally offer at least one opening time per day. During the semester break, we try to cover as many dates as possible. Unfortunately, this doesn't always work out and when it does, it tends to be spontaneous.

In general, however, student representatives are very often there, even if no opening hours are scheduled. If you're on campus anyway, you can of course just drop by. The safe method is a quick call to [0721 / 608-43783](tel:+4972160843783). If someone is there, you can logically also ask how long someone will be on site.

By the way, you can also do an opening time yourself, we are happy about every helping hand. Anyone can do it and it's not a difficult task. Just ask the next time you visit!
{{%/ accordion-item %}}



{{% accordion-item "Why are the protocols/exams not available online?" %}}
[We once wrote a detailed Funke article about this](/funke_161.pdf).
In short: for data protection reasons, we are unfortunately not allowed to send protocols and exams digitally. However, many lecturers now upload old exams in the respective Ilias course. 

{{%/ accordion-item %}}




{{% accordion-item "I am now in the 3rd/5th Bachelor semester. Where can I find out about elective options?" %}}
**ETIT:** The following specializations are currently offered in the Bachelor's degree:
- General Electrical Engineering
- Automation, Robotics & Systems Engineering
- Electrical Energy Systems and Electromobility
- Information and Communication
- Microelectronics, Photonics and Quantum Technologies

In addition, there is an elective area that is completely independent of the specialization. You can find out which modules are available in the module handbook.

The choice of specialization in the Bachelor's degree has no influence on the choice of specialization in the Master's degree. It is therefore perfectly possible to change your choice of specialization when you move on to the Master's degree.

**MEDT:** In the module handbook you will find a list of all electives that you can choose in the MEDT Bachelor. This choice has no influence on your subsequent Master's degree. If you are considering switching to ETIT for your Master's degree, it may make sense to choose the compulsory modules from ETIT as electives that are not already included in the MEDT compulsory area. 

**MIT:** At MIT, the specialization subjects are divided into compulsory elective modules and supplementary modules. You can find out how to select them and when supplementary modules can be credited in the [Module Handbook](https://www.sle.kit.edu/vorstudium/bachelor-mechatronik-informationstechnik_13982.php) under "Specialization in Mechatronics".

Otherwise, you can attend the elective module presentation in the 2nd semester. It is organized by the BPA and deans of the interfaculty mechatronics degree programme and is always announced a few weeks beforehand in the lecture. If you missed this event, you can view the slides under Elective modules in the specialization. 
{{%/ accordion-item %}}




{{% accordion-item "Where can I find out about organizational matters and connect with fellow students?" %}}
If you have questions about exercise sheets, the (non-)taking place of lectures or the lecture hall allocation for the next exam, you can join the WhatsApp, Signal or Telegram group of your year (if you have an account).

These are usually administered by the orientation week organization. There, you and your fellow students can answer each other's questions. 

There is also a Discord server that is moderated by fellow ETIT students. Please contact us if you would like to have the link for this server.
{{%/ accordion-item %}}




{{% accordion-item "Which books can you recommend for subject X?" %}}
Of course, there is no general answer to this question. However, it is generally a good idea if you want to prepare for an exam with additional materials, as most official lecture notes have a high scientific standard and are therefore difficult to understand if you have not yet fully understood the topic.

However, you should also be aware that official materials from the lecturer will later form the basis of the exam and you should therefore not rely entirely on recommended books.

Most lecturers present their recommendations for additional literature in the first lecture or exercise. You could therefore look for the bibliography in the course slides. If you can't find anything there, you can also ask the lecturer for book recommendations by email.
{{%/ accordion-item %}}




{{% accordion-item "I arrived in Karlsruhe too late and was therefore unable to take part in the orientation week or courses in the first few weeks of lectures. What important information did I miss?" %}}
In the first lectures, usually only organizational matters such as access passwords to teaching materials or recommendations for additional literature are discussed.

If you have such questions, it is best to contact the responsible lecturer. You can find their e-mail address on the respective institute homepage.

The student council is also the right point of contact for questions about your studies. If you have questions about various courses (e.g. the ETIT workshop or the Matlab assignment in LEN), you are welcome to drop by the student council and ask us your questions. After making an appointment, we can also offer you a subsequent campus tour, during which we will show you various important facilities on campus.
{{%/ accordion-item %}}

{{% accordion-item "When are examination results published?" %}}
Exam results are usually published on the Campus Portal within 4 to 6 weeks after the exam. To be notified directly when a new grade is published, you can activate various e-mail notifications in the Campus Portal menu under Personal Information > Notifications.

From time to time, it can happen that the correction of an exam may take longer due to unforeseen circumstances. However, if the grade is important for your further progress in your studies, you can contact the lecturers to ask for a "4.0 certificate". This confirms that you have passed the exam with at least a 4.0. The correct grade will then be entered after the correction.

If the correction takes much longer than 6 weeks, you are welcome to contact us. 
{{%/ accordion-item %}}

{{% accordion-item "I'm in Karlsruhe, the orientation week hasn't started yet and the afternoon after the preliminary course is long. What should I do?" %}}
If the weather is good, it's always a good idea to go to the castle park! Unfortunately, sports courses are not a good place to go at the moment because you are not insured for them so close to the end of the semester.

If you just want to get to know people in a relaxed atmosphere, then drop by the AKK or Z10.
{{%/ accordion-item %}}

{{% accordion-item "When are the exam dates for the semester after next online?" %}}
In cooperation with the BPA and MPA, we try to plan the examination dates as early as possible. As soon as they are fixed, you will find them on the faculty homepage for the ETIT Bachelor, MEDT Bachelor and ETIT Master as well as in the showcase in front of the student council. You can find all MIT exam dates [here](https://www.ak-mit.vs.kit.edu/docs/organisatorisches/#klausurtermine)
{{%/ accordion-item %}}

{{% accordion-item "When are the vacations?" %}}
The Christmas vacations at KIT are from 24.12 (Christmas Eve) up to and including 06.01 (Epiphany). No events should take place during this period.
Depending on which days of the week this falls on, events may also be canceled before or after. This depends on the institutes or the organizers themselves.
In the summer semester, the Pentecost week (i.e. the week starting on Whit Monday) is free. Some institutes offer excursions during this week, for which you can register. 
You can find the exact semester and lecture-free periods [here](https://www.sle.kit.edu/imstudium/termine-fristen.php)

Incidentally, there are also lectures before Christmas that do not cover exam-relevant material.


{{%/ accordion-item %}}


{{% accordion-item "I have failed the oral re-examination. What happens now?" %}}
This is unpleasant at first, but nothing that others have not already gone through. Although you have lost your right to take exams for this course (and not only at KIT), there are still ways in which you can continue your studies.

Such cases are dealt with by the examination boards (Bachelor's and Master's examination boards), on which students from the student council also sit. It is best to contact them directly for advice. 

The contact addresses are [bpa@fs-etit.kit.edu](mailto:bpa@fs-etit.kit.edu) for matters relating to Bachelor's degree programs and [mpa@fs-etit.kit.edu](mailto:mpa@fs-etit.kit.edu) for Master's degree programs.

**Important:** For orientation examinations, the following from the examination regulations (paragraph 8, section 2) must be observed: "Students who have not successfully passed the orientation examinations, including any resits, by the end of the third semester lose their right to take examinations in the degree program, unless they are not responsible for missing the deadline; the examination board will decide on this at the student's request. A second repetition of the orientation examinations is excluded."
{{%/ accordion-item %}}

{{% accordion-item "I have failed a repeat examination. What happens now?" %}}
That's not too bad at first, it happens to some of us. You now have to take an oral re-examination, but this cannot be graded better than 4.0. To help you prepare, you can pick up oral examination transcripts from us. There are repeat examinations that tend to take place more often than others. We therefore have more records of these than of others.

As some of us student representatives have also gone through the procedure ourselves, don't be afraid to come to us and listen to their experiences.
{{%/ accordion-item %}}

{{% accordion-item "I have failed an exam. What happens now?" %}}
That's not too bad at first, it happens to almost all of us. You can simply write the re-examination in the following semester or listen to the lecture again the semester after and then write it again. However, there are a few special rules for the orientation exams. These must be passed by the end of the 3rd semester. It is not possible to apply for a second retake!
{{%/ accordion-item %}}




{{% accordion-item "When is the orientation week? What can I expect?" %}}
The orientation phase takes place in the week before the start of lectures, participation is not compulsory but advisable.

You will be prepared for your studies there: All kinds of information about the course itself, the KIT and so on. You will also get to know the campus, the city, evening activities and most importantly - your fellow students.
{{%/ accordion-item %}}



{{% accordion-item "I am looking for an internship, thesis, job, and so on, do you have any vacancies?" %}}
We regularly post offers/requests in our showcases at the student council. There is also an e-mail distribution list [angebote@fs-etit.kit.edu](mailto:angebote@fs-etit.kit.edu) for interested parties, which regularly receives requests. More information can be found here.

Posters and notices can be sent directly to us [indu@fs-etit.kit.edu](mailto:indu@fs-etit.kit.edu) or in DIN A4 format (please as PDF) to [innen@fs-etit.kit.edu](mailto:innen@fs-etit.kit.edu). These will only be printed in black and white.
{{%/ accordion-item %}}



{{% accordion-item "I am not in Karlsruhe and need exams/protocols for XY. What can I do?" %}}
In very well justified exceptional cases, we can also send exams and transcripts to your home by post. In addition to the printing costs, there are also shipping costs, which you can pay either on site or by bank transfer.

In principle, we must NOT make exams and transcripts available digitally! 
{{%/ accordion-item %}}




{{% accordion-item "Which preliminary courses should I attend before starting my studies?" %}}
The preliminary course for a rough overview or to brush up on math takes place in the two weeks before the semester starts. As most students go there, you can meet lots of new people, especially there.

If it's been a while since your A-levels or you didn't study math and physics intensively at school, there are some very detailed preliminary courses offered by the MINT-Kolleg Baden-Württemberg. You have to register for these.
{{%/ accordion-item %}}



{{% accordion-item "Do you have protocols for XY?" %}}
You can find a list of available protocols [here](/en/service/minutes/).

If there are no minutes available for the lecture, we are very sorry - but we only have limited influence on what is offered. However, you can share your knowledge by filling out the protocol form. Your fellow students will thank you! You can either send the protocol to the [Klausurenreferat](mailto:klausuren@fs-etit.kit.edu) or bring it to the student council (you will also receive a small thank you).

To avoid redundancy and scattering, the transcripts of lectures from other faculties (MACH/CIW, INFO, etc.) should be available from the relevant student council.
{{%/ accordion-item %}}

{{% accordion-item "How do I find the courses for my Master in ETIT/MEDT/MIT?" %}}
We've written a document for this question. You'll find it [here](/How_to_ILIAS_and_CAS.pdf) (only available in english).
{{%/ accordion-item %}}

{{</ accordion >}}
