---
title: Participate
menu:
    main:
        parent: about-us
        weight: 3
---

## Why, why, why ...
The O-Phase was great, you met great new people, you survived the first exam phase with past exam papers and you got to know the party culture on and around campus.

Working with students directly is an important, but not sole, part of student council responsibilities. An even more important aspect is the contact with faculty and professors. The setting of exam dates, co-determination of exam modalities or the appointment of new professors who maintain or raise the teaching standard is also part of it. 

Also the exchange with other student councils from Germany, Switzerland and Austria is part of the regular events we attend or organize. 
 
## How can you participate?
Just ask the next time you buy exams or when the AKK is too crowded for a coffee drop by for a cup of coffee with us. Additionally you can come to our regular student council meetings on Wednesdays at 6:00 pm.

We are happy about everyone who supports us and wants to get to know the student council. 
Due to the fact that we are all  students ourselves with uni, sports, social life and other obligations we all have a very good understanding of limited time and motivation. 
 
## Payment?!
Money is overrated, our payment method is special: **experience and contacts**.

Jokes aside. We are students who give out volunteer help in our free time. All the money we get our hands on is used to make everyone's studies even better. What we can actually offer you is quite unironically what is written above. You get the possibility to realize yourself and to try things you always wanted to try but never dared to do for fear of breaking something.

It doesn't matter if you want to cut exams in the computer department, work at the counter and sell exams, work as an appeals committee for new subjects or do big things like the main organization of the Bachelor O-Phase. We have the right project for every demand and people who have done it before and can supervise you. 

If you are looking for specific keywords for your resume, you will learn with us: Teamwork, Time Management, Risk Assessment, Public Speaking, Negotiation Skills, Project Management and any other fancy buzzword that you can throw into a conversation 😉
 
## Join us!
Have we aroused your interest? Then just drop by at one of our opening hours or at the student council meeting.

All you need is interest and motivation to get involved in new topics.
