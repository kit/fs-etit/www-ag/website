---
title: 'What is the students council doing?'
menu:
    main:
        parent: about-us
        weight: 2
slug: what-we-do
---

## Resorts
Within the student council there are different resorts which do different kind of work for the student council. These are for the most part independent and report to the student council meeting from time to time.
The members of a resort are elected at the student council meeting. However, this is not a big hurdle, so that the entry into a resort is very easy. Also, you can (and should) join a resort before the election. 
We are always looking for new members for the resorts. So if you are interested in a resort and would like to help out, just get in touch with the contact address of the resort.

## BPA and MPA
BPA and MPA stand for Bachelor and Master Examination Committee ("Bachelor- und Masterprüfungsausschuss"). We are the student representatives in both committees and have an advisory vote there.
If you have any problems with your studies, specific questions about your studies or if you want to submit an application to the BPA/MPA, please feel free to contact us.
If you would like to make an appointment for a consultation, just contact us or send us an email.

Contact: [BPA resort](mailto:bpa@fs-etit.kit.edu), [MPA resort](mailto:mpa@fs-etit.kit.edu)

## Exams resort
One of the most important services of the student council is the sale of exams and minutes. We take care to always provide you with the latest old exams and make sure that the exams always have a proper format. In addition, we maintain the exam collections and thus enable a smooth sale of exams and protocols.
We always need help, especially when exams are due to be cut again.

Contact: [Exams resort](mailto:klausuren@fs-etit.kit.edu)

## O-Phase Team
O-Phase is one of the most fun weeks in the entire degree program. Here the first-year students get to know the university, go partying together and simply have a brilliant week together.
Behind the O-Phase is an Orga Team composed of the main Orga and students from the 3rd semester. The planning starts at the beginning of the summer semester. The so-called main orga has the overview of all events that take place during the week. The events themselves are organized by subgroups. During the week many cool helpers are needed to support the different events and help the orga to make the week run as smoothly as possible.
If you are interested in helping out during the next O-Phase, just send us a short mail.

Contact: [o-phase](mailto:o-phase@fs-etit.kit.edu)

## Equal Opportunity resort
The Equal Opportunity resort stands up for all those who feel discriminated against at the university and during their everyday study life because of their origin, gender, sexual orientation or physical handicap.
Currently, we organize an international Stammtisch (regulars' table) at least once a semester to which all students of electrical engineering, mechatronics and medical engineering are invited. The Stammtisch is intended to provide an opportunity for international and German students to exchange ideas and get to know each other better.

Contact: [equal opportunities](mailto:chancengleichheit@fs-etit.kit.edu)

## Funke / Public Relations
You've probably heard of our [Instagram](https://www.instagram.com/fs.etit.kit/) account, that and any other representation of the student council to the outside world is part of this resort. We publish "Der Funke", our student council newspaper, and additionally maintain the student council's ILIAS course. Here and on the Instagram account we always bring you up to date and inform you about parties and actions of the student council.
Among the tasks one takes over are for example:
- **Funke:** Once a semester publish the Funke including evaluation forms.
- Funke:** Interview faculty and recruit institutes.
- **Instagram:** Revise posts and publish a story once a week on FS session topics.
- **ILIAS / Instagram / Funke:** Recruit about the latest events of the student council.
If you have new ideas or would like to contribute to the student council's social media presence, please get in touch.

Contact: [Funke/Public Relations](mailto:funke@fs-etit.kit.edu)

## www-AG
The WWW-AG takes care of the maintenance and further development of the student council website.  This includes the content as well as the technical side.
We are looking forward to new helpers. Tasks are for example
- Rewrite single texts
- Fixing error messages
- Maintaining and updating the website
- Extending the website with own works (php, scripts etc.)

Contact: [WWW-AG](mailto:wwwag@fs-etit.kit.edu)

## Computer resort
The computer resort takes care of the digital well-being of the student council. This includes the maintenance, development and administration of the servers and computers. We take care of the software, manage the student council accounts and maintain the server. If you are interested, please come to our meeting at the Fachschaft at 6pm on Monday.

Contact: [Computer resort](mailto:rechner@fs-etit.kit.edu)

## Industry resort
The industry resort tries to establish contacts with companies that are potential employers for our graduates.

For this purpose, excursions are organized for the students, in order to create a small insight into the companies, but also into the later professional field. This can also help some students to decide in which area of electrical engineering and information technology they would like to specialize. The industry resort strives to offer a wide range of courses in order to meet the needs of all students.

If you as a company are interested or would like to offer an excursion, we would be very pleased if you would contact us.
The contact person is the industry resort in almost all cases. Exceptions to this are, for example, notices in our showcases, where the Interior resort is responsible.

Contact: [Industry resort](mailto:indu@fs-etit.kit.edu)

## Interior resort
The interior resort takes care of the premises of the student council. We keep an eye on the anteroom to make sure it doesn't fall into chaos, take care of the furnishing of the student council and are responsible for the notices in front of the student council.
If you have ideas for setting up the student council or are just looking for a good resort to start with, feel free to contact us.

Contact: [Interior resort](mailto:innen@fs-etit.kit.edu)


## Book resort
We take care of the library of the student council. Even if there is no more lending, there is still the possibility to read technical literature in the student council and to work with the readings there.

Contact: [Book Desk](mailto:buecher@fs-etit.kit.edu)

## External resort
In order not to lose contact with other resorts of electrical engineering, the external resort is responsible for strengthening these relationships. In addition, it also takes care of matters that might arise between university groups and the student council. 

Among the tasks that one takes on as an external referent are:
- to inform oneself in time about the next "Bundesfachschaftenstagung der Elektrotechnik " (BuFaTa) and to forward the information to the Fachschaft members.
- find student representatives for the BuFaTa ET in time and take care of their delegation in time.
- Continue or establish cooperation with university groups and associations with electrical engineering affinity (e.g. VDE).
We would be pleased if you are interested in filling this position. 

If you are also interested or if you have any ideas for cooperation as a university group, you are welcome to send us an [email](mailto:info@fs-etit.kit.edu).




