---
title: Mitmachen
menu:
    main:
        parent: about-us
        weight: 3
---

## Wieso, weshalb, warum ...
Die O-Phase war super, du hast tolle neue Leute kennengelernt, mit Altklausuren hast du die erste Klausurenphase überlebt und du hast die Party-Kultur auf und um den Campus kennengelernt.

Arbeit mit den Studierenden direkt ist ein wichtiger, jedoch nicht alleiniger Teil, der Fachschaftsverpflichtungen. Eine fast sogar wichtigerer Aspekt ist der Kontakt zu Fakultät und Professor:innen. Das Legen der Klausurtermine, Mitbestimmung von Prüfungsmodalitäten oder das Berufen neuer Professor:innen die den Lehrstandard halten oder anheben, gehört unter anderem auch dazu. 

Auch der Austausch mit anderen Fachschaften aus Deutschland, der Schweiz und Österreich ist Teil der regelmäßigen Events die wir besuchen oder organisieren. 
 
## Wie kannst du mitmachen?
Einfach mal nachfragen beim nächsten Mal Klausuren kaufen oder, wenn das AKK zu voll ist, bei uns auf einen Kaffee vorbei schauen. Man kann auch jederzeit bei einer unserer Sitzungen Mittwochs um 18:00 Uhr vorbeischauen.

Wir freuen uns über jeden der uns unterstützt und die Fachschaftsarbeit kennenlernen möchte. 
Dadurch, dass wir alle selbst auch Studierende mit Uni, Sport, Sozialleben und WG-Putzplänen sind haben alle ein sehr gutes Verständnis für begrenzte Zeit und Motivation. 
 
## Bezahlung?!
Geld wird überbewertet, unsere Bezahlmethode ist besonders: **Erfahrung und Bekanntschaften**.

Spaß bei Seite. Wir sind Studierende die in ihrer Freizeit ehrenamtliche Hilfe austeilen. Sämtliches Geld welches wir doch mal in die Finger bekommen wird dazu verwendet das Studium Aller noch besser zu machen. Was wir dir tatsächlich anbieten können ist ganz unironisch das was oben steht. Du bekommst bei uns die Möglichkeit dich selbst zu verwirklichen und Dinge auszuprobieren die du schon immer mal probieren wolltest dich aber nie getraut hast aus Angst dabei was kaputt zu machen.

Egal ob Klausuren schneiden im Rechnerreferat, eine Thekenschicht mit Klausurenverkauf, Berufungskomission für neue Professor:innen oder ganz große Sachen wie Hauptorganisation der Bachelor O-Phase. Wir haben für jedes Verlangen das richtige Projekt und Menschen die das davor auch schon mal gemacht haben und dich betreuen und einlernen können. 

Solltest du nach konkreten Keywords für deinen Lebenslauf suchen lernst du bei uns: Teamarbeit, Zeitmanagement, Risikoeinschätzung, Public Speaking, Negotiation Skills, Project Management und alles sonst was sich richtig "fancy" ins Englische übersetzen lässt 😉
 
## Mach mit!
Haben wir deine Interesse geweckt? Dann kommt einfach bei einer unserer Öffnungszeiten oder zur Fachschaftssitzung vorbei.

Voraussetzung dafür ist nur gute Laune und Motivation, dich in neuen Themen einzubringen.

