---
title: Was macht die Fachschaft?
menu:
    main:
        parent: about-us
        weight: 2
slug: was-machen-wir
---

## Referate
Innerhalb der Fachschaft gibt es verschiedene Referate, die bestimmte Aufgaben der Fachschaft erfüllen. 
Die Mitglieder eines Referats werden Referent:innen genannt und auf der Sitzung gewählt. Dies ist allerdings keine große Hürde, sodass der Einstieg in ein Referat sehr einfach vonstatten geht. Außerdem kannst du auch so ein Referat unterstützen, wnen du dich dafür interessierst. 
Es werden auch immer Mitglieder für die Referate gesucht. Wenn dir also ein Referat gefällt und du Lust hast mitzuhelfen, melde dich einfach unter der Kontaktadresse des Referats.
 
## BPA und MPA
BPA und MPA stehen abkürzend für Bachelor- und Masterprüfungsausschuss. Wir sind die studentischen Vertreter:innen in den beiden Ausschüssen und haben dort eine beratende Stimme.
Bei Problemen im Studium, spezielleren Studienfragen und Antragsstellungen an den BPA/MPA kannst du dich gerne an uns wenden.
Wenn du einen Beratungstermin ausmachen möchtest, dann melde dich einfach bei uns oder schreib eine Mail.

Kontakt: [BPA-Referat](mailto:bpa@fs-etit.kit.edu), [MPA-Referat](mailto:mpa@fs-etit.kit.edu)

## Klausurreferat
Eine der wichtigsten Dienstleistungen der Fachschaft ist der Klausuren- und Protokollverkauf. Wir kümmern uns darum, euch immer die neusten Altklausuren zur Verfügung zu stellen und sorgen dafür, dass die Klausuren immer ein ordentliches Format haben. Wir pflegen zusätzlich die Klausurensammlungen und ermöglichen so einen reibungslosen Klausuren- und Protokollverkauf.
Wir brauchen immer Hilfe, besonders wenn wieder das Klausurenschneiden ansteht.

Kontakt: [Klausurreferat](mailto:klausuren@fs-etit.kit.edu)


## O-Phasen Team
Die O-Phase ist eine der spaßigsten Wochen im gesamten Studium. Hier lernen die Erstis die Uni kennen, gehen zusammen feiern und haben einfach eine geniale Woche zusammen.
Hinter der O-Phase steht ein Orga Team zusammengesetzt aus der Hauptorga und Studierenden aus dem 3. Semester. Die Planung startet bereits zum Beginn des Sommersemesters. Die sogenannte Hauptorga hat den Überblick über alle Events die in der Woche statt finden. Die einzelnen Events selber werden aber von Untergruppen organisiert. Während der Woche werden dann viele coole Helfer:innen gebraucht, die die verschiedenen Events mit betreuen und die Orga dabei unterstützen die Woche so reibungslos wie möglich verlaufen zu lassen.
Wenn ihr selber Lust habt bei der nächsten O-Phase mitzuwirken schreibt uns doch einfach eine kurze Mail.

Kontakt: [O-Phase](mailto:o-phase@fs-etit.kit.edu)

## Chancengleichheitsreferat
Das Chancengleicheitsreferat setzt sich für all diejenigen ein, die sich aufgrund von Herkunft, Geschlecht, sexueller Orientierung oder körperlicher Beeinträchtigung an der Universität und während des Studienalltags diskriminiert fühlen.
Zurzeit organisieren wir mindestens einmal im Semester einen internationalen Stammtisch zu dem alle Studierenden der Elektrotechnik, Mechatronik und Medizintechnik eingeladen sind. Der Stammtisch soll die Möglichkeit bieten, dass sich internationale und deutsche Studierende untereinander austauschen und besser kennen lernen können.

Kontakt: [Chancengleichheit](mailto:chancengleichheit@fs-etit.kit.edu)


## Funke / Öffentlichkeitsarbeit
Du hast sicherlich schon von unserem [Instagram](https://www.instagram.com/fs.etit.kit/)-Account gehört, der und jede andere Repräsentation der Fachschaft nach Außen ist Teil dieses Referats. Wir veröffentlichen "Der Funke", unsere Fachschafts-Zeitung und betreuen zusätzlich den ILIAS-Kurs der Fachschaft. Hier und auf dem Instagram-Account bringen wir euch immer auf den neusten Stand und informieren über Feste und Aktionen der Fachschaft.
Unter der Aufgaben die man übernimmt stehen zum Beispiel:
- **Funke:** Einmal im Semester den Funke samt Evaluationsbögen veröffentlichen.
- **Funke:** Interview mit Lehrkräften führen sowie Institute anwerben.
- **Instagram:** Beiträge überarbeiten und einmal in der Woche eine Story zur FS-Sitzungsthemen veröffentlichen.
- **ILIAS / Instagram / Funke:** Über die neuesten Veranstaltungen der Fachschaft anwerben.
Wenn ihr neue Ideen habt oder gerne zu der Social-Media Präsenz der Fachschaft beitragen wollt, dann meldet euch.

Kontakt: [Funke/Öffentlichkeitsarbeit](mailto:funke@fs-etit.kit.edu)

## www-AG
Die WWW-AG kümmert sich um die Instandhaltung und Weiterentwicklung der Fachschaftswebsite.  Das sowohl von der inhaltlichen, wie auch von der technischen Seite.
Wir freuen uns sehr über neue Helfer:innen. Aufgaben sind zum Beispiel:
- Einzelne Texte neu schreiben
- Fehlermeldungen beheben
- Pflege und Update der Webseite
- Die Webseite durch Eigenwerke (php, Skripte etc.) erweitern

Kontakt: [WWW-AG](mailto:wwwag@fs-etit.kit.edu)

## Rechner-Referat
Im Rechner-Referat wird sich um das digitale Wohlbefinden der Fachschaft gekümmert. Das umfasst die Wartung, Weiterentwicklung und Administration der Fachschafts Server und Rechner. Wir kümmern um die Software, verwalten die Fachschafts-Accounts und pflegen den Server. Wenn du Interesse hast, dann komm doch um 18 Uhr am Montag zu unserer Sitzung in die Fachschaft.

Kontakt: [Rechner-Referat](mailto:rechner@fs-etit.kit.edu)

## Industrie-Referat
Das Industriereferat bemüht sich um Kontakte zu Firmen, die potentielle Arbeitgeber:innen unserer Absolvent:innen sind.

Dazu werden Exkursionen für die Studierenden organisiert, um einen kleinen Einblick in die Unternehmen, aber auch in das spätere Berufsfeld zu schaffen. Einigen Studierenden kann hierdurch auch die Entscheidung erleichtert werden, auf welchem Teilgebiet der Elektro- und Informationstechnik sie sich spezialisieren wollen. Das Industriereferat bemüht sich dabei, ein breites Angebot zu bieten, um möglichst allen Studierendeninteressen gerecht zu werden.

Falls Sie als Unternehmen Interesse haben oder eine Exkursion anbieten wollen, würden wir uns sehr freuen, wenn Sie Kontakt mit uns aufnehmen.
Ansprechpartner ist das Industriereferat in fast allen Fällen. Ausnahmen bilden zum Beispiel Aushänge in unseren Schaukästen, dort ist das Innenreferat zuständig.

Kontakt: [Industrie-Referat](mailto:indu@fs-etit.kit.edu)


## Innen-Referat
Beim Innenreferat wird sich um die Räumlichkeiten der Fachschaft gekümmert. Wir haben ein Auge darauf, dass der Vorraum nicht im Chaos versinkt, kümmern uns um die Einrichtung der Fachschaft und sind verantwortlich für die Aushänge vor der Fachschaft.
Wenn du Ideen für die Einrichtung der Fachschaft hast oder nur ein gutes Referat zum Einstieg suchst, dann melde dich gerne bei uns.

Kontakt: [Innen-Referat](mailto:innen@fs-etit.kit.edu)


## Bücherreferat
Wir kümmern uns um die Bestandsbibliothek der Fachschaft. Auch wenn kein Verleih mehr stattfindet, gibt es dennoch die Möglichkeit, Fachliteratur in der Fachschaft zu lesen und dort mit den Lektüren zu arbeiten.

Kontakt: [Bücherreferat](mailto:buecher@fs-etit.kit.edu)

## Außen-Referat
Um den Kontakt mit anderen Fachschaften der Elektrotechnik nicht zu verlieren, ist das Außen-Referat dafür zuständig, diese Beziehungen zu verstärken. Außerdem kümmert es sich auch um Angelegenheiten, die zwischen Hochschulgruppen und die Fachschaft zustande kommen könnten. 

Unter der Aufgaben, die man als Außen-Referent:in übernimmt, stehen:
- sich rechtzeitig über das nächste Bundesfachschaftenstagung der Elektrotechnik (BuFaTa) zu informieren und den Fachschaftler:innen die Informationen weiterleiten.
- rechtzeitig Fachschaftler:innen für die BuFaTa ET finden und sich rechtzeitig um ihre Entsendung kümmern.
- Kooperation mit Hochschulgruppen und elektrotechnisch affinen Vereinen (z.B. VDE) weiter zu betreiben oder neu zu schließen.
Wir freuen uns, wenn ihr Interesse an dem Besetzen dieses Referates habt. 

Wenn ihr darüber hinaus Interesse habt oder als Hochschulgruppe irgendwelche Kooperationsideen habt, könnt ihr uns gerne eine [Mail](mailto:info@fs-etit.kit.edu) schreiben.

