---
title: Was ist die Fachschaft?
menu:
    main:
        parent: about-us
        weight: 1
---

{{< img src="about-us/fs-foto.webp" alt="Gruppenfoto Fachschaftsmitglieder" align="right" pl="1rem" width="60%" >}}



## Was ist die Fachschaft?
Die Fachschaft bildet die Schnittstelle zwischen Studierenden und der Fakultät. 
Im eigentlichen Sinn werden als Fachschaft alle Studierenden eines Fachbereiches bezeichnet.

Üblicherweise ist jedoch die Ehrenamtlichen organisierte Interessenvertretung gemeint, wenn man von Fachschaft spricht.

Die aktive Fachschaft bilden derzeit ca. 40 aktive Studierende der Elektro- und Informationstechnik, Medizintechnik und Mechatronik, die ehrenamtlich für die Studierenden und Studieninteressierten da sind.

Diese Studiernden sind in einer Vielzahl an Referaten organisiert. Ein Referat ist dabei eine Art Abteilung der Fachschaft, die sich um einen bestimmten Arbeitsbereich kümmert.

## Die Fachschaftsleitung
Die Fachschaftsleitung leitet das Tagesgeschäft der Fachschaft. Sie besteht aus zwei Personen:
- dem Fachschaftsleiter (FASL)
- dem stellvertretenden Fachschaftsleiter (FISL)

Der FASL ist üblicherweise derjenige, der bei der Wahl der verfassten Studierendenschaft (VS-Wahl) der Fachschaften die meisten Stimmen erhalten hat. Sonst wird er, genauso wir der FISL, aus der Mitte des Fachschaftsvorstands gewählt. Er oder der FISL sind üblicherweise die Leiter der Fachschaftssitzung, können die Aufgabe aber an jeden Fachschaftler weitergeben. Der FISL wird vom Fachschafts-Vorstand gewählt.

## Was machen wir?
Die Fachschaft ist die erste Anlaufstelle für Fragen und Probleme rund um das Studium.

Als Interessensvertretung vertritt die Fachschaft die Anliegen der Studierenden in verschiedenen Gremien der Universität. In der Praxis bietet sie aber viel mehr:
- Wir bieten während dem Semester regelmäßig Öffnungszeiten.
- Solange die Fachschaft offen ist, werden Altklausuren und Prüfungsprotokolle zur Prüfungsvorbereitung angeboten.
- Jedes Jahr werden von uns die Orientierungsphasen für Bachelor- und Masterstudierende organisiert, die Studienanfängern den Campus und das Studentenleben näherbringen.
- Jedes Jahr werden Feste, sowie Exkursionen zu verschiedenen Instituten und Firmen, von uns organisiert.
- Jeden Mittwoch um 18 Uhr findet eine Fachschaftssitzung statt. In dieser werden anstehende Entscheidungen getroffen. Dabei haben alle eingeschriebenen Studierende der Fakultät Elektro- und Informationstechnik ein Stimmrecht, du also auch!
- Und noch viel mehr!

## Wie findest du uns?
Die von der Fachschaft organisierten Veranstaltungen werden oft durch Hörsaalwerbung, unserem Instagram-Account (@fs.etit.kit) und dem E-Mail-Verteiler vorgestellt. Außerdem gibt es einen ILIAS-Kurs der Fachschaft Elektro- und Informationstechnik, auf dem du alle wichtigen Informationen rund ums Studium findest, sogar Ausschreibungen für HiWi-Stellen und Praktika. Zusätzlich veröffentlichen wir in regelmäßigen Abständen unsere Fachschaftszeitung: „der Funke“, welchen wir kostenlos an die Studierenden verteilen.
