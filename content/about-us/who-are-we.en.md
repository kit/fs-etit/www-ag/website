---
title: What is the student council?
menu:
    main:
        parent: about-us
        weight: 1
---

{{< img src="about-us/fs-foto.webp" alt="Gruppenfoto Fachschaftsmitglieder" align="right" pl="1rem" width="60%" >}}



## What is the student council?
The student council is a student organization responsible for all students within the faculty for Electrical Engineering and Information Technologies.
In the actual sense, all students of a faculty are referred to as the student council. The students have a representation of interests, which is also called student council.

Usually the representation of interests organized by volunteers is meant when one speaks of student council.

The active student council is currently made up of about 40 active students of Electrical Engineering and Information Technology, Medical Engineering and Mechatronics, who are there on a voluntary basis for the students and those interested in studying.

These students are organized in a variety of departments. A department is a kind of department of the student council, which takes care of a certain area of work.

## The Student Council Administration
The student council administration manages the day-to-day business of the student council. It consists of two persons
- the head of the student council (FASL)
- the deputy head of the student council (FISL)

The FASL is usually the person who has received the most votes in the Student Council election. Otherwise, just like the FISL, he or she is elected from among the student council members. He or the FISL are usually the leaders of the student council meeting, but can pass on the task to any student council member. The FISL is elected by the student council executive committee.

## What do we do?
The student council is the first point of contact for questions and problems concerning your studies.

As a representation of interests, the student council represents the concerns of the students in various committees of the university. In practice, however, it offers much more:
- We offer regular opening hours during the semester.
- As long as the student council is open, we offer old exams and exam protocols for exam preparation.
- Every year we organize the orientation phases for Bachelor and Master students, which introduce first-year students to the campus and student life.
- Every year we organize parties, as well as excursions to different institutes and companies.
- Every Wednesday at 6 pm there is a student council meeting. In this meeting, decisions are made. All enrolled students of the Faculty of Electrical Engineering and Information Technology have the right to vote, so do you!
- And much more!

## How do you find us?
The events organized by the student council are often presented through lecture hall advertisements, our Instagram account (@fs.etit.kit) and the email distribution list. There is also an ILIAS course of the student council Electrical Engineering and Information Technology, where you can find all important information about studying, even announcements for HiWi positions and internships. In addition, we regularly publish our student council newspaper: "der Funke", which we distribute to students free of charge.
