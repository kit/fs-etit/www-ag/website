module gitlab.kit.edu/fs-etit/www-ag/website

go 1.19

require (
	github.com/gohugoio/hugo-mod-bootstrap-scss/v5 v5.20300.20200 // indirect
	github.com/gohugoio/hugo-mod-jslibs-dist/popperjs/v2 v2.21100.20000 // indirect
	github.com/lipis/flag-icons v7.2.3+incompatible // indirect
	github.com/twbs/bootstrap v5.3.3+incompatible // indirect
	gitlab.kit.edu/kit/fs-etit/www-ag/website-modules/available-minutes v0.0.0-20241212124818-4bdabf99a42d // indirect
	gitlab.kit.edu/kit/fs-etit/www-ag/website-modules/calendar v0.0.0-20240826202516-2dfbf6c688a2 // indirect
	gitlab.kit.edu/kit/fs-etit/www-ag/website-modules/timetables v0.0.0-20241104180134-feb1754e437e // indirect
)
