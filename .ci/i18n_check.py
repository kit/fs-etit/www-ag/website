#!/usr/bin/env python3
import argparse
import operator
import tomllib
from functools import reduce  # forward compatibility for Python 3
from pathlib import Path


def _directory(s):
    path = Path(s).expanduser()
    if path.is_dir():
        return path
    else:
        raise argparse.ArgumentTypeError(f'path {path} is not a directory')


def get_from_dict(data: dict, key_list: list):
    return reduce(operator.getitem, key_list, data)


def set_in_dict(data: dict, key_list: list, value):
    get_from_dict(data, key_list[:-1])[key_list[-1]] = value


def load_config(config_path: Path) -> dict:
    if config_path.is_dir():
        default_config_path = config_path / '_default'
        config_files = list(default_config_path.iterdir())

        main_config_file = default_config_path / 'hugo.toml'
        config_files.remove(main_config_file)
        with open(main_config_file, 'rb') as fs:
            config = tomllib.load(fs)

        for f in config_files:
            with open(f, 'rb') as fs:
                partial_config = tomllib.load(fs)
            config_tree = f.stem.split('.')
            set_in_dict(config, config_tree, partial_config)

        return config
    else:
        raise ValueError(f'config path {config_path} not valid')


def get_lang_suffix(file: Path):
    suffixes = file.suffixes
    if len(suffixes) == 1:
        return None
    else:
        return suffixes[-2].strip('.')


def main():
    parser = argparse.ArgumentParser(description='checks if page content is available in all configured languages')
    parser.add_argument('--content', default='./content', help='Content Directory', type=_directory)
    parser.add_argument('--config', default='./config', help='HUGO Config File or Path', type=Path)

    args = parser.parse_args()

    content_directory: Path
    content_directory = args.content.expanduser()
    config_path: Path
    config_path = args.config.expanduser()

    config = load_config(config_path)
    languages = list(config.get('languages', []).keys())
    default_language = config['defaultContentLanguage']

    markdown_files = [path.relative_to(content_directory) for path in content_directory.glob('**/*.md')]
    existing_files = {lang: [] for lang in languages}
    for file in markdown_files:
        lang = get_lang_suffix(file)
        file_path_stripped = str(file).removesuffix('.md')
        if lang is None:
            existing_files[default_language].append(file_path_stripped)
        elif lang in languages:
            file_path_stripped = file_path_stripped.removesuffix(f'.{lang}')
            existing_files[lang].append(file_path_stripped)
        else:
            raise ValueError(f'found unexpeced language code {lang} in filename {file}')

    all_files = set([file for language_map in existing_files.values() for file in language_map])
    missing = {lang: [] for lang in languages}

    for lang, lang_files in existing_files.items():
        for file in all_files:
            if file not in lang_files:
                if lang != default_language:
                    file = f'{file}.{lang}'
                missing[lang].append(f'{file}.md')

    if not any(file_list for file_list in missing.values()):
        exit(0)

    print('Missing translated files:')
    for lang, file_list in missing.items():
        if file_list:
            print(f'{lang}:')
            for file in file_list:
                print(f'  - {file}')
    exit(1)


if __name__ == '__main__':
    main()
