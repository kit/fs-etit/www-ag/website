---
default:
  image: registry.gitlab.com/fs-etit/container/hugo:latest
  cache:  # Cache modules using lock file
    key:
      files:
        - package-lock.json
    paths:
      - .npm/

workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event'
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

stages:
  - test
  - build
  - deploy

woke:
  image: getwoke/woke:latest
  stage: test
  script:
    - woke -c https://gitlab.kit.edu/kit/fs-etit/rechner/ci-config/-/raw/main/woke.yml

i18n:
  image: python:3.11-bookworm
  stage: test
  script:
    - python3 .ci/i18n_check.py
  allow_failure: true

.build:
  stage: build
  variables:
    GIT_STRATEGY: clone
    GIT_DEPTH: 0
  artifacts:
    paths:
      - public
    expire_in: 1 day
  script:
    - npm ci --cache .npm --prefer-offline
    - hugo
    - npx --cache .npm --no-install pagefind --site public

build-staging:
  extends: .build
  environment:
    name: staging/$CI_COMMIT_REF_NAME
    url: https://$STAGING_URL/$CI_COMMIT_REF_NAME
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

build-production:
  extends: .build
  environment: production
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

.deploy:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  before_script:
    - apt-get update -y
    - command -v rsync || ( apt-get install -y rsync )
    - command -v ssh-agent || ( apt-get install -y openssh-client )
    - eval $(ssh-agent -s)
    - echo "${DEPLOY_KEY}" | tr -d '\r' | ssh-add -
  script:
    - rsync -avz --delete --no-owner --no-perms --no-group -e "ssh -o StrictHostKeyChecking=no" public/ ${DEPLOY_USER}@${DEPLOY_TARGET}:${DEPLOY_PATH}

deploy-staging:
  extends: .deploy
  environment:
    name: staging/$CI_COMMIT_REF_NAME
    url: ${HUGO_BASEURL}
    on_stop: cleanup-staging
  needs:
    - job: build-staging
      artifacts: True
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"

deploy-production:
  extends: .deploy
  environment: production
  needs:
    - job: build-production
      artifacts: True
  rules:
    - if: $CI_MERGE_REQUEST_IID
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

cleanup-staging:
  stage: deploy
  variables:
    GIT_STRATEGY: none
  environment:
    name: staging/$CI_COMMIT_REF_NAME
    action: stop
  before_script:
    - apt-get update -y
    - command -v ssh-agent || ( apt-get install -y openssh-client )
    - eval $(ssh-agent -s)
    - echo "${DEPLOY_KEY}" | tr -d '\r' | ssh-add -
  script:
    - ssh -o StrictHostKeyChecking=no ${DEPLOY_USER}@${DEPLOY_TARGET} "rm -rf ${DEPLOY_PATH}"
  allow_failure: True
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
      when: manual
